## DOMPETKU
It is all about API DOMPETKU

## Table of Content

-[Tech Stacks](#tech-stack)
-[Features DOMPETKU](#features)
-[How to use](#how-to-user)

# Tech Stacks
- Text Editor (VS Code)
- Golang (Echo-Framework)
- Postman (Testing API)
- Docker (Containerize the application)
- Gitlab (Gitlab)
- If you are windows user, please install wsl2

# Features
|Features Name                         | Authorization   |     Method                   | Description                                                  |
|--------------------------------------|-----------------|------------------------------|--------------------------------------------------------------|
| User SignUp                          | No              |                              | Feature for create user                                      |
| User SignIn                          | No              |                              | Feature for login into the application                       |
| User SignOut                         | Yes             | Bearer Token Using JWT TOKEN | Feature for logout from application                          |
| Update User Profile                  | Yes             | Bearer Token Using JWT TOKEN | Feature for updating user profile                            |
| Create Conventional Invoice          | Yes             | Bearer Token Using JWT TOKEN | Feature for creating conventional invoice                    |
| Get All Conventional Invoice         | Yes             | Bearer Token Using JWT TOKEN | Feature for getting all conventional invoice                 |
| Get Conventional Invoice by Name     | Yes             | Bearer Token Using JWT TOKEN | Feature for getting conventional invoice by name             |
| Get Conventional Invoice by Tenor    | Yes             | Bearer Token Using JWT TOKEN | Feature for getting conventional invoice by tenor            |
| Get Conventional Invoice by Grade    | Yes             | Bearer Token Using JWT TOKEN | Feature for getting conventional invoice by grade            |
| Get Conventional Invoice by Rate     | Yes             | Bearer Token Using JWT TOKEN | Feature for getting conventional invoice by rate             |
| Update Conventional Invoice          | Yes             | Bearer Token Using JWT TOKEN | Feature for updating conventional invoice                    |
| Delete Conventional Invoice          | Yes             | Bearer Token Using JWT TOKEN | Feature for deleting conventional invoice                    |
| Create Conventional OSF              | Yes             | Bearer Token Using JWT TOKEN | Feature for creating conventional OSF                        |
| Get All Conventional OSF             | Yes             | Bearer Token Using JWT TOKEN | Feature for getting all conventional OSF                     |
| Get Conventional OSF by Name         | Yes             | Bearer Token Using JWT TOKEN | Feature for getting conventional OSF by name                 |
| Get Conventional OSF by Tenor        | Yes             | Bearer Token Using JWT TOKEN | Feature for getting conventional OSF by tenor                |
| Get Conventional OSF by Grade        | Yes             | Bearer Token Using JWT TOKEN | Feature for getting conventional OSF by grade                |
| Get Conventional OSF by Rate         | Yes             | Bearer Token Using JWT TOKEN | Feature for getting conventional OSF by rate                 |
| Create Productive Invoice            | Yes             | Bearer Token Using JWT TOKEN | Feature for creating Productive invoice                      |
| Get All Productive Invoice           | Yes             | Bearer Token Using JWT TOKEN | Feature for getting all Productive invoice                   |
| Get Productive Invoice by Name       | Yes             | Bearer Token Using JWT TOKEN | Feature for getting Productive invoice by name               |
| Get Productive Invoice by Tenor      | Yes             | Bearer Token Using JWT TOKEN | Feature for getting Productive invoice by tenor              |
| Get Productive Invoice by Grade      | Yes             | Bearer Token Using JWT TOKEN | Feature for getting Productive invoice by grade              |
| Get Productive Invoice by Rate       | Yes             | Bearer Token Using JWT TOKEN | Feature for getting Productive invoice by rate               |
| Update Productive Invoice            | Yes             | Bearer Token Using JWT TOKEN | Feature for updating Productive invoice                      |
| Delete Productive Invoice            | Yes             | Bearer Token Using JWT TOKEN | Feature for deleting Productive invoice                      |
| Create Financing                     | Yes             | Bearer Token Using JWT TOKEN | Feature for creating financing                               |
| Get All Financing                    | Yes             | Bearer Token Using JWT TOKEN | Feature for getting all Financing                            |
| Get Financing by Name                | Yes             | Bearer Token Using JWT TOKEN | Feature for getting Financing by name                        |
| Get Financing by Sub                 | Yes             | Bearer Token Using JWT TOKEN | Feature for getting Financing by grade                       |
| Update Financing                     | Yes             | Bearer Token Using JWT TOKEN | Feature for updating Financing                               |
| Delete Financing                     | Yes             | Bearer Token Using JWT TOKEN | Feature for deleting Financing                               |
| Create Reksadana                     | Yes             | Bearer Token Using JWT TOKEN | Feature for creating Reksadana                               |
| Get All Reksadana                    | Yes             | Bearer Token Using JWT TOKEN | Feature for getting all Reksadana                            |
| Get Reksadana by Name                | Yes             | Bearer Token Using JWT TOKEN | Feature for getting Reksadana by name                        |
| Get Reksadana by Return              | Yes             | Bearer Token Using JWT TOKEN | Feature for getting Reksadana by Return                      |
| Update Reksadana                     | Yes             | Bearer Token Using JWT TOKEN | Feature for updating Reksadana                               |
| Delete Reksadana                     | Yes             | Bearer Token Using JWT TOKEN | Feature for deleting Reksadana                               |
| Create SBN                           | Yes             | Bearer Token Using JWT TOKEN | Feature for creating SBN                                     |
| Get All SBN                          | Yes             | Bearer Token Using JWT TOKEN | Feature for getting all SBN                                  |
| Get SBN by Name                      | Yes             | Bearer Token Using JWT TOKEN | Feature for getting SBN by name                              |
| Get SBN by Tenor                     | Yes             | Bearer Token Using JWT TOKEN | Feature for getting SBN by tenor                             |
| Get SBN by Type                      | Yes             | Bearer Token Using JWT TOKEN | Feature for getting SBN by Type                              |
| Get SBN by Rate                      | Yes             | Bearer Token Using JWT TOKEN | Feature for getting SBN by rate                              |
| Update SBN                           | Yes             | Bearer Token Using JWT TOKEN | Feature for updating SBN                                     |
| Delete SBN                           | Yes             | Bearer Token Using JWT TOKEN | Feature for deleting SBN                                     |

# How to User this API
1. All of tech stack must be ready.
2. Clone this code from gitlab repository [Dompetku](https://gitlab.com/pebruwantoro/dompetku/-/tree/master)
- Next Steps if you clone from repository github
3. Open terminal, wsl2 or powershell and go to directory that contains these files
4. Build image using <docker build --tag image_name:tag_name .> , dont forget to check docker image ls -a to see image had successfully created
5. Build docker compose using <docker-compose up>, dont forget to check docker container ls -a to see container had successfully created and running
6. If this apps already have run, you are able to test the API using postman
7. See folder routes and open router to configure url params in postman
8. You can use all of features API this apps, for the first step you have to create an account, login into system and dont forget to copy JWT token that you are able to use for authorization, because most of features needs authorization access.

You can see this image in DockerHub [Dompetku](https://hub.docker.com/repository/registry-1.docker.io/donpebru/dompetku-v1/tags?page=1&ordering=last_updated)