package controllers

import (
	"app/dompetku/lib/database"
	"app/dompetku/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

// Controller for creating Conventional OSF
func CreateConventionalOSFController(c echo.Context) error {
	var input models.ConventionalOSF
	// binding data input from users
	c.Bind(&input)
	// inserting input data into database
	osf, err := database.CreateConventionalOSF(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not create data",
		})
	}
	return c.JSON(http.StatusOK, osf)
}

func CreateConventionalOSFControllerTesting() echo.HandlerFunc {
	return CreateConventionalOSFController
}

// Controller for Showing all of Conventional OSF data from database
func GetAllConventionalOSFController(c echo.Context) error {
	// requesting to show all of Conventional OSF data
	osf, err := database.GetAllConventionalOSF()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not fetch data",
		})
	}
	return c.JSON(http.StatusOK, osf)
}

func GetAllConventionalOSFControllerTesting() echo.HandlerFunc {
	return GetAllConventionalOSFController
}

// Controller for Showing all of Conventional OSF data filter by name from database
func GetConventionalOSFByNameController(c echo.Context) error {
	// binding osf name from params
	name := c.Param("osf_name")
	// requesting to show Conventional OSF data filter by name
	osf, err := database.GetConventionalOSFByName(name)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(osf) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, osf)
}

func GetConventionalOSFByNameControllerTesting() echo.HandlerFunc {
	return GetConventionalOSFByNameController
}

// Controller for Showing all of Conventional OSF data filter by tenor from database
func GetConventionalOSFByTenorController(c echo.Context) error {
	// binding osf tenor from params
	tenor, err := strconv.Atoi(c.Param("osf_tenor"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show Conventional OSF data filter by tenor
	osf, err := database.GetConventionalOSFByTenor(tenor)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(osf) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, osf)
}

func GetConventionalOSFByTenorControllerTesting() echo.HandlerFunc {
	return GetConventionalOSFByTenorController
}

// Controller for Showing all of Conventional OSF data filter by grade from database
func GetConventionalOSFByGradeController(c echo.Context) error {
	// binding osf grade from params
	grade := c.Param("osf_grade")
	// requesting to show Conventional osf data filter by grade
	osf, err := database.GetConventionalOSFByGrade(grade)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(osf) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, osf)
}

func GetConventionalOSFByGradeControllerTesting() echo.HandlerFunc {
	return GetConventionalOSFByGradeController
}

// Controller for Showing all of Conventional OSF data filter by rate from database
func GetConventionalOSFByRateController(c echo.Context) error {
	// binding osf rate from params
	rate, err := strconv.Atoi(c.Param("osf_rate"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show Conventional OSF data filter by rate
	osf, err := database.GetConventionalOSFByRate(rate)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(osf) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, osf)
}

func GetConventionalOSFByRateControllerTesting() echo.HandlerFunc {
	return GetConventionalOSFByRateController
}

// Controller for updating Conventional OSF data
func UpdateConventionalOSFController(c echo.Context) error {
	var input models.ConventionalOSF
	// binding osf rate from params
	id, err := strconv.Atoi(c.Param("osf_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// request for finding data filter by id
	get_osf, err := database.GetConventionalOSFById(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not get data",
		})
	}
	input = get_osf
	// binding update data from users
	c.Bind(&input)
	// update and save new data into database
	updated_osf, err := database.UpdateConventionalOSF(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not update data",
		})
	}
	return c.JSON(http.StatusOK, updated_osf)
}

func UpdateConventionalOSFControllerTesting() echo.HandlerFunc {
	return UpdateConventionalOSFController
}

// Controller for deleting Conventional OSF data
func DeleteConventionalOSFController(c echo.Context) error {
	// binding osf rate from params
	id, err := strconv.Atoi(c.Param("osf_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// deleting and record the deleting time using soft delete
	deleted_osf, err := database.DeleteConventionalOSF(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not delete data",
		})
	}
	return c.JSON(http.StatusOK, deleted_osf)
}

func DeleteConventionalOSFControllerTesting() echo.HandlerFunc {
	return DeleteConventionalOSFController
}
