package controllers

import (
	"app/dompetku/config"
	"app/dompetku/constant"
	"app/dompetku/lib/database"
	"app/dompetku/middlewares"
	"app/dompetku/models"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/stretchr/testify/assert"
)

var (
	Financing1 = models.Financing{
		ID:        1,
		Name:      "Financing1",
		Count:     3,
		Sub:       "OSF",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	Financing2 = models.Financing{
		ID:        2,
		Name:      "Financing2",
		Count:     30,
		Sub:       "Invoice",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateFinancingController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Financing{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// input dummy data into body input
	body, _ := json.Marshal(Financing1)
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/financing")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(CreateFinancingControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputFinancing
	}
	var response Response
	req_body2 := res.Body.String()
	json.Unmarshal([]byte(req_body2), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("POST /financing", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Financing1.Name, response.Name)
		assert.Equal(t, Financing1.Count, response.Count)
		assert.Equal(t, Financing1.Sub, response.Sub)
	})
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestGetAllFinancingController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Financing{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Financing database
	database.CreateFinancing(Financing1)
	database.CreateFinancing(Financing2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/financing")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetAllFinancingControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputFinancing
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /financing", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Financing1.Name, response[0].Name)
		assert.Equal(t, Financing1.Count, response[0].Count)
		assert.Equal(t, Financing1.Sub, response[0].Sub)
		assert.Equal(t, Financing2.Name, response[1].Name)
		assert.Equal(t, Financing2.Count, response[1].Count)
		assert.Equal(t, Financing2.Sub, response[1].Sub)
	})
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestGetFinancingByNameController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Financing{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Financing database
	database.CreateFinancing(Financing1)
	database.CreateFinancing(Financing2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/financing/name/:financing_name")
	context.SetParamNames("financing_name")
	context.SetParamValues("Financing2")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetFinancingByNameControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputFinancing
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /financing/name/:financing_name", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Financing2.Name, response[0].Name)
		assert.Equal(t, Financing2.Count, response[0].Count)
		assert.Equal(t, Financing2.Sub, response[0].Sub)
	})
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestGetFinancingBySubController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Financing{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Financing database
	database.CreateFinancing(Financing1)
	database.CreateFinancing(Financing2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/financing/sub/:financing_sub")
	context.SetParamNames("financing_sub")
	context.SetParamValues("Invoice")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetFinancingBySubControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputFinancing
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /financing/sub/:financing_sub", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Financing2.Name, response[0].Name)
		assert.Equal(t, Financing2.Count, response[0].Count)
		assert.Equal(t, Financing2.Sub, response[0].Sub)
	})
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestUpdateFinancingController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Financing{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Financing database
	database.CreateFinancing(Financing1)
	database.CreateFinancing(Financing2)
	//get financing by id
	get_financing, _ := database.GetFinancingById(1)
	// updated financing database
	updated_financing := get_financing
	updated_financing.Name = "FinancingBaru"
	updated_financing.Count = 30000000000
	updated_financing.Sub = "Productive Invoice"
	// insert using body request
	body, _ := json.Marshal(updated_financing)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	user.Token = token
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPut, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/financing/:financing_id")
	context.SetParamNames("financing_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(UpdateFinancingControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputFinancing
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if not equal test case will be passed
	*/
	t.Run("PUT /Financing/:Financing_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.NotEqual(t, Financing1.Name, response.Name)
		assert.NotEqual(t, Financing1.Count, response.Count)
		assert.NotEqual(t, Financing1.Sub, response.Sub)
	})
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestDeleteFinancingController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Financing{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Financing database
	database.CreateFinancing(Financing1)
	database.CreateFinancing(Financing2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodDelete, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/financing/:financing_id")
	context.SetParamNames("financing_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(DeleteFinancingControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputFinancing
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	t.Run("DELETE /Financing/:Financing_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Financing1.Name, response.Name)
		assert.Equal(t, Financing1.Count, response.Count)
		assert.Equal(t, Financing1.Sub, response.Sub)
	})
	db.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}
