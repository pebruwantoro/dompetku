package controllers

import (
	"app/dompetku/lib/database"
	"app/dompetku/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

// Controller for creating Conventional Invoice
func CreateConventionalInvoiceController(c echo.Context) error {
	var input models.ConventionalInvoice
	// binding data input from users
	c.Bind(&input)
	// inserting input data into database
	invoice, err := database.CreateConventionalInvoice(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not create data",
		})
	}
	return c.JSON(http.StatusOK, invoice)
}

func CreateConventionalInvoiceControllerTesting() echo.HandlerFunc {
	return CreateConventionalInvoiceController
}

// Controller for Showing all of Conventional Invoice data from database
func GetAllConventionalInvoiceController(c echo.Context) error {
	// requesting to show all of Conventional Invoice data
	invoices, err := database.GetAllConventionalInvoices()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not fetch data",
		})
	}
	return c.JSON(http.StatusOK, invoices)
}

func GetAllConventionalInvoiceControllerTesting() echo.HandlerFunc {
	return GetAllConventionalInvoiceController
}

// Controller for Showing all of Conventional Invoice data filter by name from database
func GetConventionalInvoiceByNameController(c echo.Context) error {
	// binding invoice name from params
	name := c.Param("invoice_name")
	// requesting to show Conventional Invoice data filter by name
	invoice, err := database.GetConventionalInvoiceByName(name)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(invoice) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, invoice)
}

func GetConventionalInvoiceByNameControllerTesting() echo.HandlerFunc {
	return GetConventionalInvoiceByNameController
}

// Controller for Showing all of Conventional Invoice data filter by tenor from database
func GetConventionalInvoiceByTenorController(c echo.Context) error {
	// binding invoice tenor from params
	tenor, err := strconv.Atoi(c.Param("invoice_tenor"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show Conventional Invoice data filter by tenor
	invoice, err := database.GetConventionalInvoiceByTenor(tenor)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(invoice) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, invoice)
}

func GetConventionalInvoiceByTenorControllerTesting() echo.HandlerFunc {
	return GetConventionalInvoiceByTenorController
}

// Controller for Showing all of Conventional Invoice data filter by grade from database
func GetConventionalInvoiceByGradeController(c echo.Context) error {
	// binding invoice grade from params
	grade := c.Param("invoice_grade")
	// requesting to show Conventional Invoice data filter by grade
	invoice, err := database.GetConventionalInvoiceByGrade(grade)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(invoice) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, invoice)
}

func GetConventionalInvoiceByGradeControllerTesting() echo.HandlerFunc {
	return GetConventionalInvoiceByGradeController
}

// Controller for Showing all of Conventional Invoice data filter by rate from database
func GetConventionalInvoiceByRateController(c echo.Context) error {
	// binding invoice rate from params
	rate, err := strconv.Atoi(c.Param("invoice_rate"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show Conventional Invoice data filter by rate
	invoice, err := database.GetConventionalInvoiceByRate(rate)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(invoice) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, invoice)
}

func GetConventionalInvoiceByRateControllerTesting() echo.HandlerFunc {
	return GetConventionalInvoiceByRateController
}

// Controller for updating Conventional Invoice data
func UpdateConventionalInvoiceController(c echo.Context) error {
	var input models.ConventionalInvoice
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("invoice_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// request for finding data filter by id
	get_invoice, err := database.GetConventionalInvoiceById(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not get data",
		})
	}
	input = get_invoice
	// binding update data from users
	c.Bind(&input)
	// update and save new data into database
	updated_invoice, err := database.UpdateConventionalInvoice(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not update data",
		})
	}
	return c.JSON(http.StatusOK, updated_invoice)
}

func UpdateConventionalInvoiceControllerTesting() echo.HandlerFunc {
	return UpdateConventionalInvoiceController
}

// Controller for deleting Conventional Invoice data
func DeleteConventionalInvoiceController(c echo.Context) error {
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("invoice_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// deleting and record the deleting time using soft delete
	deleted_invoice, err := database.DeleteConventionalInvoice(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not delete data",
		})
	}
	return c.JSON(http.StatusOK, deleted_invoice)
}

func DeleteConventionalInvoiceControllerTesting() echo.HandlerFunc {
	return DeleteConventionalInvoiceController
}
