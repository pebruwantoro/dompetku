package controllers

import (
	"app/dompetku/config"
	"app/dompetku/constant"
	"app/dompetku/lib/database"
	"app/dompetku/middlewares"
	"app/dompetku/models"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/stretchr/testify/assert"
)

var (
	SBN1 = models.SBN{
		ID:        1,
		Name:      "SBN1",
		Amount:    100000,
		Tenor:     12,
		Rate:      3,
		Type:      "Financing",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	SBN2 = models.SBN{
		ID:        2,
		Name:      "SBN2",
		Amount:    1000000,
		Tenor:     13,
		Rate:      4,
		Type:      "Financing",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateSBNController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.SBN{})           // create new table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// input dummy data into body input
	body, _ := json.Marshal(SBN1)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/sbn")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(CreateSBNControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputSBN
	}
	var response Response
	req_body2 := res.Body.String()
	json.Unmarshal([]byte(req_body2), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("POST /sbn", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, SBN1.Name, response.Name)
		assert.Equal(t, SBN1.Amount, response.Amount)
		assert.Equal(t, SBN1.Tenor, response.Tenor)
		assert.Equal(t, SBN1.Rate, response.Rate)
		assert.Equal(t, SBN1.Type, response.Type)
	})
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestGetAllSBNController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.SBN{})           // create new table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional SBN database
	database.CreateSBN(SBN1)
	database.CreateSBN(SBN2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/sbn")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetAllSBNControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputSBN
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /sbn", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, SBN1.Name, response[0].Name)
		assert.Equal(t, SBN1.Amount, response[0].Amount)
		assert.Equal(t, SBN1.Tenor, response[0].Tenor)
		assert.Equal(t, SBN1.Rate, response[0].Rate)
		assert.Equal(t, SBN1.Type, response[0].Type)
		assert.Equal(t, SBN2.Name, response[1].Name)
		assert.Equal(t, SBN2.Amount, response[1].Amount)
		assert.Equal(t, SBN2.Tenor, response[1].Tenor)
		assert.Equal(t, SBN2.Rate, response[1].Rate)
		assert.Equal(t, SBN2.Type, response[1].Type)
	})
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestGetSBNByNameController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.SBN{})           // create new table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional SBN database
	database.CreateSBN(SBN1)
	database.CreateSBN(SBN2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/sbn/name/:sbn_name")
	context.SetParamNames("sbn_name")
	context.SetParamValues("SBN2")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetSBNByNameControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputSBN
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /sbn/name/:sbn_name", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, SBN2.Name, response[0].Name)
		assert.Equal(t, SBN2.Amount, response[0].Amount)
		assert.Equal(t, SBN2.Tenor, response[0].Tenor)
		assert.Equal(t, SBN2.Rate, response[0].Rate)
		assert.Equal(t, SBN2.Type, response[0].Type)
	})
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestGetSBNByTenorController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.SBN{})           // create new table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional SBN database
	database.CreateSBN(SBN1)
	database.CreateSBN(SBN2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/sbn/tenor/:sbn_tenor")
	context.SetParamNames("sbn_tenor")
	context.SetParamValues("13")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetSBNByTenorControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputSBN
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /sbn/tenor/sbn_tenor", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, SBN2.Name, response[0].Name)
		assert.Equal(t, SBN2.Amount, response[0].Amount)
		assert.Equal(t, SBN2.Tenor, response[0].Tenor)
		assert.Equal(t, SBN2.Rate, response[0].Rate)
		assert.Equal(t, SBN2.Type, response[0].Type)
	})
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestGetSBNByTypeController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.SBN{})           // create new table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional SBN database
	database.CreateSBN(SBN1)
	database.CreateSBN(SBN2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/sbn/type/:sbn_type")
	context.SetParamNames("sbn_type")
	context.SetParamValues("Financing")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetSBNByTypeControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputSBN
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /sbn/type/:sbn_type", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, SBN2.Name, response[1].Name)
		assert.Equal(t, SBN2.Amount, response[1].Amount)
		assert.Equal(t, SBN2.Tenor, response[1].Tenor)
		assert.Equal(t, SBN2.Rate, response[1].Rate)
		assert.Equal(t, SBN2.Type, response[1].Type)
	})
}

func TestGetSBNByRateController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.SBN{})           // create new table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional SBN database
	database.CreateSBN(SBN1)
	database.CreateSBN(SBN2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_sbn/rate/:sbn_rate")
	context.SetParamNames("sbn_rate")
	context.SetParamValues("3")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetSBNByRateControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputSBN
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_SBN/rate/:SBN_rate", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, SBN1.Name, response[0].Name)
		assert.Equal(t, SBN1.Amount, response[0].Amount)
		assert.Equal(t, SBN1.Tenor, response[0].Tenor)
		assert.Equal(t, SBN1.Rate, response[0].Rate)
		assert.Equal(t, SBN1.Type, response[0].Type)
	})
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestUpdateSBNController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.SBN{})           // create new table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional SBN database
	database.CreateSBN(SBN1)
	database.CreateSBN(SBN2)
	get_SBN, _ := database.GetSBNById(1)
	updated_SBN := get_SBN
	updated_SBN.Name = "SBNBaru"
	updated_SBN.Amount = 1000000000000000
	updated_SBN.Tenor = 24
	updated_SBN.Rate = 6
	updated_SBN.Type = "NonFinancing"
	body, _ := json.Marshal(updated_SBN)
	// create JWT toke for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPut, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/sbn/:sbn_id")
	context.SetParamNames("sbn_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(UpdateSBNControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputSBN
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	new_SBN := response
	new_SBN.Name = "SBNBaru"
	new_SBN.Amount = 1000000000000000
	new_SBN.Tenor = 24
	new_SBN.Rate = 6
	new_SBN.Type = "NonFinancing"
	/*
		create a several test case to campare request to response,
		if not equal test case will be passed
	*/
	t.Run("PUT /sbn/:sbn_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.NotEqual(t, SBN1.Name, response.Name)
		assert.NotEqual(t, SBN1.Amount, response.Amount)
		assert.NotEqual(t, SBN1.Tenor, response.Tenor)
		assert.NotEqual(t, SBN1.Rate, response.Rate)
		assert.NotEqual(t, SBN1.Type, response.Type)
	})
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestDeleteSBNController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.SBN{})           // create new table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional SBN database
	database.CreateSBN(SBN1)
	database.CreateSBN(SBN2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodDelete, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/sbn/:sbn_id")
	context.SetParamNames("sbn_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(DeleteSBNControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputSBN
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("DELETE /sbn/:sbn_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, SBN1.Name, response.Name)
		assert.Equal(t, SBN1.Amount, response.Amount)
		assert.Equal(t, SBN1.Tenor, response.Tenor)
		assert.Equal(t, SBN1.Rate, response.Rate)
		assert.Equal(t, SBN1.Type, response.Type)
	})
	db.Migrator().DropTable(&models.SBN{})  // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}
