package controllers

import (
	"app/dompetku/lib/database"
	"app/dompetku/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

// Controller for creating Reksadana
func CreateReksadanaController(c echo.Context) error {
	var input models.Reksadana
	// binding data input from users
	c.Bind(&input)
	// inserting input data into database
	reksadana, err := database.CreateReksadana(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not create data",
		})
	}
	return c.JSON(http.StatusOK, reksadana)
}

func CreateReksadanaControllerTesting() echo.HandlerFunc {
	return CreateReksadanaController
}

// Controller for Showing all of Reksadana data from database
func GetAllReksadanaController(c echo.Context) error {
	// requesting to show all of Reksadana data
	reksadana, err := database.GetAllReksadana()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not fetch data",
		})
	}
	return c.JSON(http.StatusOK, reksadana)
}

func GetAllReksadanaControllerTesting() echo.HandlerFunc {
	return GetAllReksadanaController
}

// Controller for Showing all of Reksadana data filter by name from database
func GetReksadanaByNameController(c echo.Context) error {
	// binding invoice name from params
	name := c.Param("reksadana_name")
	// requesting to show Reksadana data filter by name
	reksadana, err := database.GetReksadanaByName(name)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(reksadana) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, reksadana)
}

func GetReksadanaByNameControllerTesting() echo.HandlerFunc {
	return GetReksadanaByNameController
}

// Controller for Showing all of Reksadana data filter by Return from database
func GetReksadanaByReturnController(c echo.Context) error {
	// binding invoice Return from params
	input, err := strconv.Atoi(c.Param("reksadana_return"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show Reksadana data filter by Return
	reksadana, err := database.GetReksadanaByReturn(input)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(reksadana) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, reksadana)
}

func GetReksadanaByReturnControllerTesting() echo.HandlerFunc {
	return GetReksadanaByReturnController
}

// Controller for updating Reksadana data
func UpdateReksadanaController(c echo.Context) error {
	var input models.Reksadana
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("reksadana_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// request for finding data filter by id
	get_reksadana, err := database.GetReksadanaById(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not get data",
		})
	}
	input = get_reksadana
	// binding update data from users
	c.Bind(&input)
	// update and save new data into database
	updated_reksadana, err := database.UpdateReksadana(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not update data",
		})
	}
	return c.JSON(http.StatusOK, updated_reksadana)
}

func UpdateReksadanaControllerTesting() echo.HandlerFunc {
	return UpdateReksadanaController
}

// Controller for deleting Reksadana data
func DeleteReksadanaController(c echo.Context) error {
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("reksadana_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// deleting and record the deleting time using soft delete
	deleted_reksadana, err := database.DeleteReksadana(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not delete data",
		})
	}
	return c.JSON(http.StatusOK, deleted_reksadana)
}

func DeleteReksadanaControllerTesting() echo.HandlerFunc {
	return DeleteReksadanaController
}
