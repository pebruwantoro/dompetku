package controllers

import (
	"app/dompetku/lib/database"
	"app/dompetku/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

// Controller for creating SBN
func CreateSBNController(c echo.Context) error {
	var input models.SBN
	// binding data input from users
	c.Bind(&input)
	// inserting input data into database
	sbn, err := database.CreateSBN(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not create data",
		})
	}
	return c.JSON(http.StatusOK, sbn)
}

func CreateSBNControllerTesting() echo.HandlerFunc {
	return CreateSBNController
}

// Controller for Showing all of SBN data from database
func GetAllSBNController(c echo.Context) error {
	// requesting to show all of SBN data
	sbn, err := database.GetAllSBN()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not fetch data",
		})
	}
	return c.JSON(http.StatusOK, sbn)
}

func GetAllSBNControllerTesting() echo.HandlerFunc {
	return GetAllSBNController
}

// Controller for Showing all of SBN data filter by name from database
func GetSBNByNameController(c echo.Context) error {
	// binding invoice name from params
	name := c.Param("sbn_name")
	// requesting to show SBN data filter by name
	sbn, err := database.GetSBNByName(name)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(sbn) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, sbn)
}

func GetSBNByNameControllerTesting() echo.HandlerFunc {
	return GetSBNByNameController
}

// Controller for Showing all of SBN data filter by tenor from database
func GetSBNByTenorController(c echo.Context) error {
	// binding invoice tenor from params
	tenor, err := strconv.Atoi(c.Param("sbn_tenor"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show SBN data filter by tenor
	sbn, err := database.GetSBNByTenor(tenor)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(sbn) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, sbn)
}

func GetSBNByTenorControllerTesting() echo.HandlerFunc {
	return GetSBNByTenorController
}

// Controller for Showing all of SBN data filter by rate from database
func GetSBNByRateController(c echo.Context) error {
	// binding invoice rate from params
	rate, err := strconv.Atoi(c.Param("sbn_rate"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show SBN data filter by rate
	sbn, err := database.GetSBNByRate(rate)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(sbn) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, sbn)
}

func GetSBNByRateControllerTesting() echo.HandlerFunc {
	return GetSBNByRateController
}

// Controller for Showing all of SBN data filter by type from database
func GetSBNByTypeController(c echo.Context) error {
	// binding invoice type from params
	typ := c.Param("sbn_type")
	// requesting to show SBN data filter by type
	sbn, err := database.GetSBNByType(typ)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(sbn) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, sbn)
}

func GetSBNByTypeControllerTesting() echo.HandlerFunc {
	return GetSBNByTypeController
}

// Controller for updating SBN data
func UpdateSBNController(c echo.Context) error {
	var input models.SBN
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("sbn_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// request for finding data filter by id
	get_sbn, err := database.GetSBNById(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not get data",
		})
	}
	input = get_sbn
	// binding update data from users
	c.Bind(&input)
	// update and save new data into database
	updated_sbn, err := database.UpdateSBN(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not update data",
		})
	}
	return c.JSON(http.StatusOK, updated_sbn)
}

func UpdateSBNControllerTesting() echo.HandlerFunc {
	return UpdateSBNController
}

// Controller for deleting SBN data
func DeleteSBNController(c echo.Context) error {
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("sbn_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// deleting and record the deleting time using soft delete
	deleted_sbn, err := database.DeleteSBN(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not delete data",
		})
	}
	return c.JSON(http.StatusOK, deleted_sbn)
}

func DeleteSBNControllerTesting() echo.HandlerFunc {
	return DeleteSBNController
}
