package controllers

import (
	"app/dompetku/lib/database"
	"app/dompetku/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

// Controller for creating Productive Invoice
func CreateProductiveInvoiceController(c echo.Context) error {
	var input models.ProductiveInvoice
	// binding data input from users
	c.Bind(&input)
	// inserting input data into database
	prod_invoice, err := database.CreateProductiveInvoice(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not create data",
		})
	}
	return c.JSON(http.StatusOK, prod_invoice)
}

func CreateProductiveInvoiceControllerTesting() echo.HandlerFunc {
	return CreateProductiveInvoiceController
}

// Controller for Showing all of Productive Invoice data from database
func GetAllProductiveInvoiceController(c echo.Context) error {
	// requesting to show all of Productive Invoice data
	prod_invoices, err := database.GetAllProductiveInvoices()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not fetch data",
		})
	}
	return c.JSON(http.StatusOK, prod_invoices)
}

func GetAllProductiveInvoiceControllerTesting() echo.HandlerFunc {
	return GetAllProductiveInvoiceController
}

// Controller for Showing all of Productive Invoice data filter by name from database
func GetProductiveInvoiceByNameController(c echo.Context) error {
	// binding invoice name from params
	name := c.Param("prod_invoice_name")
	// requesting to show Productive Invoice data filter by name
	prod_invoice, err := database.GetProductiveInvoiceByName(name)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(prod_invoice) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, prod_invoice)
}

func GetProductiveInvoiceByNameControllerTesting() echo.HandlerFunc {
	return GetProductiveInvoiceByNameController
}

// Controller for Showing all of Productive Invoice data filter by tenor from database
func GetProductiveInvoiceByTenorController(c echo.Context) error {
	// binding invoice tenor from params
	tenor, err := strconv.Atoi(c.Param("prod_invoice_tenor"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show Productive Invoice data filter by tenor
	prod_invoice, err := database.GetProductiveInvoiceByTenor(tenor)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(prod_invoice) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, prod_invoice)
}

func GetProductiveInvoiceByTenorControllerTesting() echo.HandlerFunc {
	return GetProductiveInvoiceByTenorController
}

// Controller for Showing all of Productive Invoice data filter by grade from database
func GetProductiveInvoiceByGradeController(c echo.Context) error {
	// binding invoice grade from params
	grade := c.Param("prod_invoice_grade")
	// requesting to show Productive Invoice data filter by grade
	prod_invoice, err := database.GetProductiveInvoiceByGrade(grade)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(prod_invoice) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, prod_invoice)
}

func GetProductiveInvoiceByGradeControllerTesting() echo.HandlerFunc {
	return GetProductiveInvoiceByGradeController
}

// Controller for Showing all of Productive Invoice data filter by rate from database
func GetProductiveInvoiceByRateController(c echo.Context) error {
	// binding invoice rate from params
	rate, err := strconv.Atoi(c.Param("prod_invoice_rate"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input data correctly",
		})
	}
	// requesting to show Productive Invoice data filter by rate
	prod_invoice, err := database.GetProductiveInvoiceByRate(rate)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(prod_invoice) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, prod_invoice)
}

func GetProductiveInvoiceByRateControllerTesting() echo.HandlerFunc {
	return GetProductiveInvoiceByRateController
}

// Controller for updating Productive Invoice data
func UpdateProductiveInvoiceController(c echo.Context) error {
	var input models.ProductiveInvoice
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("prod_invoice_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// request for finding data filter by id
	get_prod_invoice, err := database.GetProductiveInvoiceById(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not get data",
		})
	}
	input = get_prod_invoice
	// binding update data from users
	c.Bind(&input)
	// update and save new data into database
	updated_prod_invoice, err := database.UpdateProductiveInvoice(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not update data",
		})
	}
	return c.JSON(http.StatusOK, updated_prod_invoice)
}

func UpdateProductiveInvoiceControllerTesting() echo.HandlerFunc {
	return UpdateProductiveInvoiceController
}

// Controller for deleting Productive Invoice data
func DeleteProductiveInvoiceController(c echo.Context) error {
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("prod_invoice_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// deleting and record the deleting time using soft delete
	deleted_prod_invoice, err := database.DeleteProductiveInvoice(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not delete data",
		})
	}
	return c.JSON(http.StatusOK, deleted_prod_invoice)
}

func DeleteProductiveInvoiceControllerTesting() echo.HandlerFunc {
	return DeleteProductiveInvoiceController
}
