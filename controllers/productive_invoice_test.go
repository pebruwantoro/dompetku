package controllers

import (
	"app/dompetku/config"
	"app/dompetku/constant"
	"app/dompetku/lib/database"
	"app/dompetku/middlewares"
	"app/dompetku/models"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/stretchr/testify/assert"
)

var (
	prod_invoice1 = models.ProductiveInvoice{
		ID:        1,
		Name:      "Productive Invoice1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	prod_invoice2 = models.ProductiveInvoice{
		ID:        2,
		Name:      "Productive Invoice2",
		Amount:    1000000,
		Tenor:     13,
		Grade:     "B",
		Rate:      4,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateProductiveInvoiceController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
	db.AutoMigrate(&models.ProductiveInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                       // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// input dummy data into body input
	body, _ := json.Marshal(prod_invoice1)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/productive_invoice")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(CreateProductiveInvoiceControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputProductiveInvoice
	}
	var response Response
	req_body2 := res.Body.String()
	json.Unmarshal([]byte(req_body2), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("POST /productive_invoice", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, prod_invoice1.Name, response.Name)
		assert.Equal(t, prod_invoice1.Amount, response.Amount)
		assert.Equal(t, prod_invoice1.Tenor, response.Tenor)
		assert.Equal(t, prod_invoice1.Grade, response.Grade)
		assert.Equal(t, prod_invoice1.Rate, response.Rate)
	})
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
}

func TestGetAllProductiveInvoiceController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
	db.AutoMigrate(&models.ProductiveInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                       // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Productive Invoice database
	database.CreateProductiveInvoice(prod_invoice1)
	database.CreateProductiveInvoice(prod_invoice2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/productive_invoice")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetAllProductiveInvoiceControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputProductiveInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /productive_invoice", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, prod_invoice1.Name, response[0].Name)
		assert.Equal(t, prod_invoice1.Amount, response[0].Amount)
		assert.Equal(t, prod_invoice1.Tenor, response[0].Tenor)
		assert.Equal(t, prod_invoice1.Grade, response[0].Grade)
		assert.Equal(t, prod_invoice1.Rate, response[0].Rate)
		assert.Equal(t, prod_invoice2.Name, response[1].Name)
		assert.Equal(t, prod_invoice2.Amount, response[1].Amount)
		assert.Equal(t, prod_invoice2.Tenor, response[1].Tenor)
		assert.Equal(t, prod_invoice2.Grade, response[1].Grade)
		assert.Equal(t, prod_invoice2.Rate, response[1].Rate)
	})
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
}

func TestGetProductiveInvoiceByNameController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
	db.AutoMigrate(&models.ProductiveInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                       // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Productive Invoice database
	database.CreateProductiveInvoice(prod_invoice1)
	database.CreateProductiveInvoice(prod_invoice2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/productive_invoice/name/:prod_invoice_name")
	context.SetParamNames("prod_invoice_name")
	context.SetParamValues("Productive Invoice2")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetProductiveInvoiceByNameControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputProductiveInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /productive_invoice/name/:prod_invoice_name", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, prod_invoice2.Name, response[0].Name)
		assert.Equal(t, prod_invoice2.Amount, response[0].Amount)
		assert.Equal(t, prod_invoice2.Tenor, response[0].Tenor)
		assert.Equal(t, prod_invoice2.Grade, response[0].Grade)
		assert.Equal(t, prod_invoice2.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
}

func TestGetProductiveInvoiceByTenorController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
	db.AutoMigrate(&models.ProductiveInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                       // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Productive Invoice database
	database.CreateProductiveInvoice(prod_invoice1)
	database.CreateProductiveInvoice(prod_invoice2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/productive_invoice/tenor/:prod_invoice_tenor")
	context.SetParamNames("prod_invoice_tenor")
	context.SetParamValues("13")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetProductiveInvoiceByTenorControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputProductiveInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /productive_invoice/tenor/:prod_invoice_tenor", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, prod_invoice2.Name, response[0].Name)
		assert.Equal(t, prod_invoice2.Amount, response[0].Amount)
		assert.Equal(t, prod_invoice2.Tenor, response[0].Tenor)
		assert.Equal(t, prod_invoice2.Grade, response[0].Grade)
		assert.Equal(t, prod_invoice2.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
}

func TestGetProductiveInvoiceByGradeController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
	db.AutoMigrate(&models.ProductiveInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                       // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Productive Invoice database
	database.CreateProductiveInvoice(prod_invoice1)
	database.CreateProductiveInvoice(prod_invoice2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/productive_invoice/grade/:prod_invoice_grade")
	context.SetParamNames("prod_invoice_grade")
	context.SetParamValues("B")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetProductiveInvoiceByGradeControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputProductiveInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /productive_invoice/grade/:prod_invoice_grade", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, prod_invoice2.Name, response[0].Name)
		assert.Equal(t, prod_invoice2.Amount, response[0].Amount)
		assert.Equal(t, prod_invoice2.Tenor, response[0].Tenor)
		assert.Equal(t, prod_invoice2.Grade, response[0].Grade)
		assert.Equal(t, prod_invoice2.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
}

func TestGetProductiveInvoiceByRateController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
	db.AutoMigrate(&models.ProductiveInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                       // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Productive Invoice database
	database.CreateProductiveInvoice(prod_invoice1)
	database.CreateProductiveInvoice(prod_invoice2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/productive_invoice/rate/:prod_invoice_rate")
	context.SetParamNames("prod_invoice_rate")
	context.SetParamValues("3")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetProductiveInvoiceByRateControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputProductiveInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /productive_invoice/rate/:prod_invoice_rate", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, prod_invoice1.Name, response[0].Name)
		assert.Equal(t, prod_invoice1.Amount, response[0].Amount)
		assert.Equal(t, prod_invoice1.Tenor, response[0].Tenor)
		assert.Equal(t, prod_invoice1.Grade, response[0].Grade)
		assert.Equal(t, prod_invoice1.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
}

func TestUpdateProductiveInvoiceController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
	db.AutoMigrate(&models.ProductiveInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                       // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Productive Invoice database
	database.CreateProductiveInvoice(prod_invoice1)
	database.CreateProductiveInvoice(prod_invoice2)
	// get productive invoice by id
	get_invoice, _ := database.GetProductiveInvoiceById(1)
	// updated productive invoice data
	updated_invoice := get_invoice
	updated_invoice.Name = "InvoiceBaru"
	updated_invoice.Amount = 1000000000000000
	updated_invoice.Tenor = 24
	updated_invoice.Grade = "C"
	updated_invoice.Rate = 6
	body, _ := json.Marshal(updated_invoice)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPut, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/productive_invoice/:iprod_nvoice_id")
	context.SetParamNames("prod_invoice_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(UpdateProductiveInvoiceControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputProductiveInvoice
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if not equal test case will be passed
	*/
	t.Run("PUT /productive_invoice/:prod_invoice_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.NotEqual(t, prod_invoice1.Name, response.Name)
		assert.NotEqual(t, prod_invoice1.Amount, response.Amount)
		assert.NotEqual(t, prod_invoice1.Tenor, response.Tenor)
		assert.NotEqual(t, prod_invoice1.Grade, response.Grade)
		assert.NotEqual(t, prod_invoice1.Rate, response.Rate)
	})
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
}

func TestDeleteProductiveInvoiceController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
	db.AutoMigrate(&models.ProductiveInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                       // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Productive Invoice database
	database.CreateProductiveInvoice(prod_invoice1)
	database.CreateProductiveInvoice(prod_invoice2)
	token, _ := middlewares.CreateToken(int(user.ID))
	user.Token = token
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodDelete, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/productive_invoice/:prod_invoice_id")
	context.SetParamNames("prod_invoice_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(DeleteProductiveInvoiceControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputProductiveInvoice
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("DELETE /productive_invoice/:prod_invoice_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, prod_invoice1.Name, response.Name)
		assert.Equal(t, prod_invoice1.Amount, response.Amount)
		assert.Equal(t, prod_invoice1.Tenor, response.Tenor)
		assert.Equal(t, prod_invoice1.Grade, response.Grade)
		assert.Equal(t, prod_invoice1.Rate, response.Rate)
	})
	db.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})              // Delete the table to make empty table
}
