package controllers

import (
	"app/dompetku/lib/database"
	"app/dompetku/models"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

// Controller for creating Financing
func CreateFinancingController(c echo.Context) error {
	var input models.Financing
	// binding data input from users
	c.Bind(&input)
	// inserting input data into database
	financing, err := database.CreateFinancing(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not create data",
		})
	}
	return c.JSON(http.StatusOK, financing)
}

func CreateFinancingControllerTesting() echo.HandlerFunc {
	return CreateFinancingController
}

// Controller for Showing all of Financing data from database
func GetAllFinancingController(c echo.Context) error {
	// requesting to show all of Financing data
	financing, err := database.GetAllFinancing()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not fetch data",
		})
	}
	return c.JSON(http.StatusOK, financing)
}

func GetAllFinancingControllerTesting() echo.HandlerFunc {
	return GetAllFinancingController
}

// Controller for Showing all of Financing data filter by name from database
func GetFinancingByNameController(c echo.Context) error {
	// binding invoice name from params
	name := c.Param("financing_name")
	// requesting to show Financing data filter by name
	financing, err := database.GetFinancingByName(name)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(financing) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, financing)
}

func GetFinancingByNameControllerTesting() echo.HandlerFunc {
	return GetFinancingByNameController
}

// Controller for Showing all of Financing data filter by Sub from database
func GetFinancingBySubController(c echo.Context) error {
	// binding invoice Sub from params
	input := c.Param("financing_sub")
	// requesting to show Financing data filter by Sub
	financing, err := database.GetFinancingBySub(input)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not find data",
		})
	}
	if len(financing) == 0 {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "please input correctly",
		})
	}
	return c.JSON(http.StatusOK, financing)
}

func GetFinancingBySubControllerTesting() echo.HandlerFunc {
	return GetFinancingBySubController
}

// Controller for updating Financing data
func UpdateFinancingController(c echo.Context) error {
	var input models.Financing
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("financing_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// request for finding data filter by id
	get_financing, err := database.GetFinancingById(id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not get data",
		})
	}
	input = get_financing
	// binding update data from users
	c.Bind(&input)
	// update and save new data into database
	updated_financing, err := database.UpdateFinancing(input)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not update data",
		})
	}
	return c.JSON(http.StatusOK, updated_financing)
}

func UpdateFinancingControllerTesting() echo.HandlerFunc {
	return UpdateFinancingController
}

// Controller for deleting Financing data
func DeleteFinancingController(c echo.Context) error {
	// binding invoice rate from params
	id, err := strconv.Atoi(c.Param("financing_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid id",
		})
	}
	// deleting and record the deleting time using soft delete
	deleted_financing, err := database.DeleteFinancing(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "can not delete data",
		})
	}
	return c.JSON(http.StatusOK, deleted_financing)
}

func DeleteFinancingControllerTesting() echo.HandlerFunc {
	return DeleteFinancingController
}
