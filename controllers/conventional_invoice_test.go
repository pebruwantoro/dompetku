package controllers

import (
	"app/dompetku/config"
	"app/dompetku/constant"
	"app/dompetku/lib/database"
	"app/dompetku/middlewares"
	"app/dompetku/models"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/stretchr/testify/assert"
)

var (
	invoice1 = models.ConventionalInvoice{
		ID:        1,
		Name:      "Invoice1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	invoice2 = models.ConventionalInvoice{
		ID:        2,
		Name:      "Invoice2",
		Amount:    1000000,
		Tenor:     13,
		Grade:     "B",
		Rate:      4,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateConventionalInvoiceController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                         // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// input dummy data into body input
	body, _ := json.Marshal(invoice1)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_invoice")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(CreateConventionalInvoiceControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalInvoice
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("POST /conventional_invoice", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, invoice1.Name, response.Name)
		assert.Equal(t, invoice1.Amount, response.Amount)
		assert.Equal(t, invoice1.Tenor, response.Tenor)
		assert.Equal(t, invoice1.Grade, response.Grade)
		assert.Equal(t, invoice1.Rate, response.Rate)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}

func TestGetAllConventionalInvoiceController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                         // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalInvoice(invoice1)
	database.CreateConventionalInvoice(invoice2)
	// create JWT toke for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_invoice")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetAllConventionalInvoiceControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_invoice", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, invoice1.Name, response[0].Name)
		assert.Equal(t, invoice1.Amount, response[0].Amount)
		assert.Equal(t, invoice1.Tenor, response[0].Tenor)
		assert.Equal(t, invoice1.Grade, response[0].Grade)
		assert.Equal(t, invoice1.Rate, response[0].Rate)
		assert.Equal(t, invoice2.Name, response[1].Name)
		assert.Equal(t, invoice2.Amount, response[1].Amount)
		assert.Equal(t, invoice2.Tenor, response[1].Tenor)
		assert.Equal(t, invoice2.Grade, response[1].Grade)
		assert.Equal(t, invoice2.Rate, response[1].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}

func TestGetConventionalInvoiceByNameController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                         // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalInvoice(invoice1)
	database.CreateConventionalInvoice(invoice2)
	// create JWT toke for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_invoice/name/:invoice_name")
	context.SetParamNames("invoice_name")
	context.SetParamValues("Invoice2")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetConventionalInvoiceByNameControllerTesting())(context) // input token for authorization user
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_invoice/name/:invoice_name", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, invoice2.Name, response[0].Name)
		assert.Equal(t, invoice2.Amount, response[0].Amount)
		assert.Equal(t, invoice2.Tenor, response[0].Tenor)
		assert.Equal(t, invoice2.Grade, response[0].Grade)
		assert.Equal(t, invoice2.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}

func TestGetConventionalInvoiceByTenorController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                         // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalInvoice(invoice1)
	database.CreateConventionalInvoice(invoice2)
	// create JWT toke for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_invoice/tenor/:invoice_tenor")
	context.SetParamNames("invoice_tenor")
	context.SetParamValues("13")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetConventionalInvoiceByTenorControllerTesting())(context)
	type Response struct {
		models.OutputConventionalInvoice
	}
	var response []Response
	req_body := res.Body.String()
	// unmarshal response into object struct
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_invoice/tenor/:invoice_tenor", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, invoice2.Name, response[0].Name)
		assert.Equal(t, invoice2.Amount, response[0].Amount)
		assert.Equal(t, invoice2.Tenor, response[0].Tenor)
		assert.Equal(t, invoice2.Grade, response[0].Grade)
		assert.Equal(t, invoice2.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}

func TestGetConventionalInvoiceByGradeController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                         // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalInvoice(invoice1)
	database.CreateConventionalInvoice(invoice2)
	// create JWT toke for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_invoice/grade/:invoice_grade")
	context.SetParamNames("invoice_grade")
	context.SetParamValues("B")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetConventionalInvoiceByGradeControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_invoice/grade/:invoice_grade", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, "Invoice2", response[0].Name)
		assert.Equal(t, 1000000, response[0].Amount)
		assert.Equal(t, 13, response[0].Tenor)
		assert.Equal(t, "B", response[0].Grade)
		assert.Equal(t, 4, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}

func TestGetConventionalInvoiceByRateController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                         // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalInvoice(invoice1)
	database.CreateConventionalInvoice(invoice2)
	// create JWT toke for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_invoice/rate/:invoice_rate")
	context.SetParamNames("invoice_rate")
	context.SetParamValues("3")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetConventionalInvoiceByRateControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalInvoice
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_invoice/rate/:invoice_rate", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, invoice1.Name, response[0].Name)
		assert.Equal(t, invoice1.Amount, response[0].Amount)
		assert.Equal(t, invoice1.Tenor, response[0].Tenor)
		assert.Equal(t, invoice1.Grade, response[0].Grade)
		assert.Equal(t, invoice1.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}

func TestUpdateConventionalInvoiceController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                         // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalInvoice(invoice1)
	database.CreateConventionalInvoice(invoice2)
	// get invoice by id
	get_invoice, _ := database.GetConventionalInvoiceById(1)
	updated_invoice := get_invoice
	// input updated data using body request
	updated_invoice.Name = "InvoiceBaru"
	updated_invoice.Amount = 1000000000000000
	updated_invoice.Tenor = 24
	updated_invoice.Grade = "C"
	updated_invoice.Rate = 6
	body, _ := json.Marshal(updated_invoice)
	// create JWT toke for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPut, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_invoice/:invoice_id")
	context.SetParamNames("invoice_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(UpdateConventionalInvoiceControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalInvoice
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if not equal test case will be passed
	*/
	t.Run("PUT /conventional_invoice/:invoice_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.NotEqual(t, invoice1.Name, response.Name)
		assert.NotEqual(t, invoice1.Amount, response.Amount)
		assert.NotEqual(t, invoice1.Tenor, response.Tenor)
		assert.NotEqual(t, invoice1.Grade, response.Grade)
		assert.NotEqual(t, invoice1.Rate, response.Rate)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}

func TestDeleteConventionalInvoiceController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalInvoice{})          // create new table
	db.AutoMigrate(&models.User{})                         // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalInvoice(invoice1)
	database.CreateConventionalInvoice(invoice2)
	// create JWT toke for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodDelete, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_invoice/:invoice_id")
	context.SetParamNames("invoice_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(DeleteConventionalInvoiceControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalInvoice
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("DELETE /conventional_invoice/:invoice_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, invoice1.Name, response.Name)
		assert.Equal(t, invoice1.Amount, response.Amount)
		assert.Equal(t, invoice1.Tenor, response.Tenor)
		assert.Equal(t, invoice1.Grade, response.Grade)
		assert.Equal(t, invoice1.Rate, response.Rate)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}
