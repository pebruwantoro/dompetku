package controllers

import (
	"app/dompetku/lib/database"
	"app/dompetku/middlewares"
	"app/dompetku/models"
	"log"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
)

// Function for encrypting user password
func PasswordEncrypt(password []byte) string {
	password_encrypt, err := bcrypt.GenerateFromPassword(password, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	return string(password_encrypt)
}

// Function for signup new user
func UserSignUpController(c echo.Context) error {
	var user models.User
	// get input data from user
	c.Bind(&user)
	// encrypt user password
	convert_password := []byte(user.Password)         // convert string to byte
	new_password := PasswordEncrypt(convert_password) // encryption process
	user.Password = new_password                      // assign encrypt password into user password
	// create a new user and save into database
	user_signup, err := database.UserSignUp(user)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not fetch data",
		})
	}
	// set output data as information
	map_user := map[string]interface{}{
		"Username": user_signup.Username,
		"Email":    user_signup.Email,
		"Password": user_signup.Password,
		"Token":    user_signup.Token,
	}
	return c.JSON(http.StatusOK, map_user)
}

// Function for SignIn user
func UserSignInController(c echo.Context) error {
	var user models.User
	// get input data from user
	c.Bind(&user)
	// get user password from database and compare with input user password
	get_password, _ := database.PasswordUser(user.Email)                              // get password from database
	err := bcrypt.CompareHashAndPassword([]byte(get_password), []byte(user.Password)) // compare input password and password from database
	if err != nil {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message": "User Unauthorized. Email or Password not equal",
		})
	}
	// sign process
	signin_data, err := database.UserSignIn(user.Email)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "can not fetch data",
		})
	}
	map_output := map[string]interface{}{
		"Username": signin_data.Username,
		"Email":    signin_data.Email,
		"Token":    signin_data.Token,
	}
	return c.JSON(http.StatusOK, map_output)
}

// Function for SignOut user
func UserSignOutController(c echo.Context) error {
	user_id, err := strconv.Atoi(c.Param("user_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid user id",
		})
	}
	// check authorization
	sign_in_user_id := middlewares.ExtractToken(c)
	if sign_in_user_id != user_id {
		return echo.NewHTTPError(http.StatusUnauthorized, "This user unauthorized to logout")
	}
	// get user by id
	sign_out_user, err := database.GetUserById(user_id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "cannot get data",
		})
	}
	// clear token and update data into database
	sign_out_user.Token = ""
	c.Bind(&sign_out_user)
	user_updated, err := database.UpdateUser(sign_out_user)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "cannot logout",
		})
	}
	map_output := map[string]interface{}{
		"Username": sign_out_user.Username,
		"Token":    user_updated.Token,
	}
	return c.JSON(http.StatusOK, map_output)
}

func UserSignOutControllerTesting() echo.HandlerFunc {
	return UserSignOutController
}

// Function for updating user
func UpdateUserController(c echo.Context) error {
	user_id, err := strconv.Atoi(c.Param("user_id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{
			"message": "invalid user id",
		})
	}
	// check authorization
	sign_in_user_id := middlewares.ExtractToken(c)
	if sign_in_user_id != user_id {
		return echo.NewHTTPError(http.StatusUnauthorized, "This user unauthorized to logout")
	}
	// get user data by id
	get_user, err := database.GetUserById(user_id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "cannot get data",
		})
	}
	// binding input update user data
	c.Bind(&get_user)
	// encrypt user password if users update their password
	convert_password := []byte(get_user.Password)     // convert string to byte
	new_password := PasswordEncrypt(convert_password) // encryption process
	get_user.Password = new_password
	// update user data
	updated_user, err := database.UpdateUser(get_user)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"message": "cannot update data",
		})
	}
	map_output := map[string]interface{}{
		"ID":       updated_user.ID,
		"Username": updated_user.Username,
		"Email":    updated_user.Email,
		"Password": updated_user.Password,
	}
	return c.JSON(http.StatusOK, map_output)
}

func UpdateUserControllerTesting() echo.HandlerFunc {
	return UpdateUserController
}
