package controllers

import (
	"app/dompetku/config"
	"app/dompetku/constant"
	"app/dompetku/lib/database"
	"app/dompetku/middlewares"
	"app/dompetku/models"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/stretchr/testify/assert"
)

var (
	Reksadana1 = models.Reksadana{
		ID:               1,
		Name:             "Reksadana1",
		Amount:           100000,
		Reksadana_Return: 3,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
	Reksadana2 = models.Reksadana{
		ID:               2,
		Name:             "Reksadana2",
		Amount:           1000000,
		Reksadana_Return: 4,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
)

func TestCreateReksadanaController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Reksadana{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// input dummy data into body input
	body, _ := json.Marshal(Reksadana1)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/reksadana")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(CreateReksadanaControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputReksadana
	}
	var response Response
	req_body2 := res.Body.String()
	json.Unmarshal([]byte(req_body2), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("POST /reksadana", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Reksadana1.Name, response.Name)
		assert.Equal(t, Reksadana1.Amount, response.Amount)
		assert.Equal(t, Reksadana1.Reksadana_Return, response.Return)
	})
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestGetAllReksadanaController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Reksadana{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Reksadana database
	database.CreateReksadana(Reksadana1)
	database.CreateReksadana(Reksadana2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	// setting request url, must match with controller request
	context := e.NewContext(req, res)
	context.SetPath("/reksadana")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetAllReksadanaControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputReksadana
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /Reksadana", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Reksadana1.Name, response[0].Name)
		assert.Equal(t, Reksadana1.Amount, response[0].Amount)
		assert.Equal(t, Reksadana1.Reksadana_Return, response[0].Return)
		assert.Equal(t, Reksadana2.Name, response[1].Name)
		assert.Equal(t, Reksadana2.Amount, response[1].Amount)
		assert.Equal(t, Reksadana2.Reksadana_Return, response[1].Return)
	})
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestGetReksadanaByNameController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Reksadana{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Reksadana database
	database.CreateReksadana(Reksadana1)
	database.CreateReksadana(Reksadana2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	// setting request url, must match with controller request
	context := e.NewContext(req, res)
	context.SetPath("/reksadana/name/:reksadana_name")
	context.SetParamNames("reksadana_name")
	context.SetParamValues("Reksadana2")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetReksadanaByNameControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputReksadana
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /reksadana/name/:reksadana_name", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Reksadana2.Name, response[0].Name)
		assert.Equal(t, Reksadana2.Amount, response[0].Amount)
		assert.Equal(t, Reksadana2.Reksadana_Return, response[0].Return)
	})
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestGetReksadanaByReturnController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Reksadana{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Reksadana database
	database.CreateReksadana(Reksadana1)
	database.CreateReksadana(Reksadana2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	// setting request url, must match with controller request
	context := e.NewContext(req, res)
	context.SetPath("/reksadana/return/:reksadana_return")
	context.SetParamNames("reksadana_return")
	context.SetParamValues("4")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetReksadanaByReturnControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputReksadana
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /reksadana/return/reksadana_return", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Reksadana2.Name, response[0].Name)
		assert.Equal(t, Reksadana2.Amount, response[0].Amount)
		assert.Equal(t, Reksadana2.Reksadana_Return, response[0].Return)
	})
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestUpdateReksadanaController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Reksadana{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Reksadana database
	database.CreateReksadana(Reksadana1)
	database.CreateReksadana(Reksadana2)
	// get reksadana by id
	get_reksadana, _ := database.GetReksadanaById(1)
	// updated reksadana data
	updated_reksadana := get_reksadana
	updated_reksadana.Name = "ReksadanaBaru"
	updated_reksadana.Amount = 1000000000000000
	updated_reksadana.Reksadana_Return = 24
	// input updated data using request body
	body, _ := json.Marshal(updated_reksadana)
	token, _ := middlewares.CreateToken(int(user.ID))
	user.Token = token
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPut, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/reksadana/:reksadana_id")
	context.SetParamNames("reksadana_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(UpdateReksadanaControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputReksadana
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if not equal test case will be passed
	*/
	t.Run("PUT /reksadana/:reksadana_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.NotEqual(t, Reksadana1.Name, response.Name)
		assert.NotEqual(t, Reksadana1.Amount, response.Amount)
		assert.NotEqual(t, Reksadana1.Reksadana_Return, response.Return)
	})
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
}

func TestDeleteReksadanaController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})      // Delete the table to make empty table
	db.AutoMigrate(&models.Reksadana{})          // create new table
	db.AutoMigrate(&models.User{})               // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Reksadana database
	database.CreateReksadana(Reksadana1)
	database.CreateReksadana(Reksadana2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodDelete, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/reksadana/:reksadana_id")
	context.SetParamNames("reksadana_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(DeleteReksadanaControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputReksadana
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("DELETE /reksadana/:reksadana_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, Reksadana1.Name, response.Name)
		assert.Equal(t, Reksadana1.Amount, response.Amount)
		assert.Equal(t, Reksadana1.Reksadana_Return, response.Return)
	})
	db.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})                // Delete the table to make empty table
}
