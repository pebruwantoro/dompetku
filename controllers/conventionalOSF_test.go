package controllers

import (
	"app/dompetku/config"
	"app/dompetku/constant"
	"app/dompetku/lib/database"
	"app/dompetku/middlewares"
	"app/dompetku/models"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/stretchr/testify/assert"
)

var (
	OSF1 = models.ConventionalOSF{
		ID:        1,
		Name:      "OSF1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	OSF2 = models.ConventionalOSF{
		ID:        2,
		Name:      "OSF2",
		Amount:    1000000,
		Tenor:     13,
		Grade:     "B",
		Rate:      4,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateConventionalOSFController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalOSF{})          // create new table
	db.AutoMigrate(&models.User{})                     // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// input dummy data into body input
	body, _ := json.Marshal(OSF1)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_osf")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(CreateConventionalOSFControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalOSF
	}
	var response Response
	req_body2 := res.Body.String()
	json.Unmarshal([]byte(req_body2), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("POST /conventional_osf", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, OSF1.Name, response.Name)
		assert.Equal(t, OSF1.Amount, response.Amount)
		assert.Equal(t, OSF1.Tenor, response.Tenor)
		assert.Equal(t, OSF1.Grade, response.Grade)
		assert.Equal(t, OSF1.Rate, response.Rate)
	})
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
}

func TestGetAllConventionalOSFController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalOSF{})          // create new table
	db.AutoMigrate(&models.User{})                     // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalOSF(OSF1)
	database.CreateConventionalOSF(OSF2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_osf")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetAllConventionalOSFControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalOSF
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_osf", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, OSF1.Name, response[0].Name)
		assert.Equal(t, OSF1.Amount, response[0].Amount)
		assert.Equal(t, OSF1.Tenor, response[0].Tenor)
		assert.Equal(t, OSF1.Grade, response[0].Grade)
		assert.Equal(t, OSF1.Rate, response[0].Rate)
		assert.Equal(t, OSF2.Name, response[1].Name)
		assert.Equal(t, OSF2.Amount, response[1].Amount)
		assert.Equal(t, OSF2.Tenor, response[1].Tenor)
		assert.Equal(t, OSF2.Grade, response[1].Grade)
		assert.Equal(t, OSF2.Rate, response[1].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
}

func TestGetConventionalOSFByNameController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalOSF{})          // create new table
	db.AutoMigrate(&models.User{})                     // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalOSF(OSF1)
	database.CreateConventionalOSF(OSF2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_osf/name/:osf_name")
	context.SetParamNames("osf_name")
	context.SetParamValues("OSF2")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetConventionalOSFByNameControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalOSF
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_osf/name/:osf_name", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, OSF2.Name, response[0].Name)
		assert.Equal(t, OSF2.Amount, response[0].Amount)
		assert.Equal(t, OSF2.Tenor, response[0].Tenor)
		assert.Equal(t, OSF2.Grade, response[0].Grade)
		assert.Equal(t, OSF2.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
}

func TestGetConventionalOSFByTenorController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalOSF{})          // create new table
	db.AutoMigrate(&models.User{})                     // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalOSF(OSF1)
	database.CreateConventionalOSF(OSF2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_osf/tenor/:osf_tenor")
	context.SetParamNames("osf_tenor")
	context.SetParamValues("13")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetConventionalOSFByTenorControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalOSF
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_osf/tenor/:osf_tenor", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, OSF2.Name, response[0].Name)
		assert.Equal(t, OSF2.Amount, response[0].Amount)
		assert.Equal(t, OSF2.Tenor, response[0].Tenor)
		assert.Equal(t, OSF2.Grade, response[0].Grade)
		assert.Equal(t, OSF2.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
}

func TestGetConventionalOSFByGradeController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalOSF{})          // create new table
	db.AutoMigrate(&models.User{})                     // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalOSF(OSF1)
	database.CreateConventionalOSF(OSF2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_osf/grade/:osf_grade")
	context.SetParamNames("osf_grade")
	context.SetParamValues("B")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetConventionalOSFByGradeControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalOSF
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_osf/grade/:osf_grade", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, OSF2.Name, response[0].Name)
		assert.Equal(t, OSF2.Amount, response[0].Amount)
		assert.Equal(t, OSF2.Tenor, response[0].Tenor)
		assert.Equal(t, OSF2.Grade, response[0].Grade)
		assert.Equal(t, OSF2.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
}

func TestGetConventionalOSFByRateController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalOSF{})          // create new table
	db.AutoMigrate(&models.User{})                     // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalOSF(OSF1)
	database.CreateConventionalOSF(OSF2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodGet, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_osf/rate/:osf_rate")
	context.SetParamNames("osf_rate")
	context.SetParamValues("3")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(GetConventionalOSFByRateControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalOSF
	}
	var response []Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("GET /conventional_osf/rate/:osf_rate", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, OSF1.Name, response[0].Name)
		assert.Equal(t, OSF1.Amount, response[0].Amount)
		assert.Equal(t, OSF1.Tenor, response[0].Tenor)
		assert.Equal(t, OSF1.Grade, response[0].Grade)
		assert.Equal(t, OSF1.Rate, response[0].Rate)
	})
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
}

func TestUpdateConventionalOSFController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalOSF{})          // create new table
	db.AutoMigrate(&models.User{})                     // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalOSF(OSF1)
	database.CreateConventionalOSF(OSF2)
	// get OSF by id
	get_osf, _ := database.GetConventionalOSFById(1)
	// updated OSF data
	updated_osf := get_osf
	updated_osf.Name = "OSFBaru"
	updated_osf.Amount = 1000000000000000
	updated_osf.Tenor = 24
	updated_osf.Grade = "C"
	updated_osf.Rate = 6
	body, _ := json.Marshal(updated_osf)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPut, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_osf/:osf_id")
	context.SetParamNames("osf_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(UpdateConventionalOSFControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalOSF
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if not equal test case will be passed
	*/
	t.Run("PUT /conventional_osf/:osf_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.NotEqual(t, OSF1.Name, response.Name)
		assert.NotEqual(t, OSF1.Amount, response.Amount)
		assert.NotEqual(t, OSF1.Tenor, response.Tenor)
		assert.NotEqual(t, OSF1.Grade, response.Grade)
		assert.NotEqual(t, OSF1.Rate, response.Rate)
	})
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
}

func TestDeleteConventionalOSFController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
	db.Migrator().DropTable(&models.User{})            // Delete the table to make empty table
	db.AutoMigrate(&models.ConventionalOSF{})          // create new table
	db.AutoMigrate(&models.User{})                     // create new table
	// inject user dummy data into database
	database.UserSignUp(user)
	// inject dummy data into Conventional Invoice table
	database.CreateConventionalOSF(OSF1)
	database.CreateConventionalOSF(OSF2)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodDelete, "/?", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/conventional_osf/:osf_id")
	context.SetParamNames("osf_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(DeleteConventionalOSFControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.OutputConventionalOSF
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("DELETE /conventional_osf/:osf_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, OSF1.Name, response.Name)
		assert.Equal(t, OSF1.Amount, response.Amount)
		assert.Equal(t, OSF1.Tenor, response.Tenor)
		assert.Equal(t, OSF1.Grade, response.Grade)
		assert.Equal(t, OSF1.Rate, response.Rate)
	})
}
