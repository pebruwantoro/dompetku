package controllers

import (
	"app/dompetku/config"
	"app/dompetku/constant"
	"app/dompetku/lib/database"
	"app/dompetku/middlewares"
	"app/dompetku/models"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/stretchr/testify/assert"
)

var (
	user = models.User{
		ID:        1,
		Username:  "user1",
		Email:     "user@gmail.com",
		Password:  "12345",
		Token:     "",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestUserSignUpController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.User{})          // create new table
	// input dummy data into body input
	body, _ := json.Marshal(user) // input data using request body
	// setting controller
	e := echo.New()
	// create a testing request body
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	// setting request url, must match with controller request
	context := e.NewContext(req, res)
	context.SetPath("/signup")
	// testing controller
	UserSignUpController(context)
	// unmarshal response into object struct
	type Response struct {
		models.User
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("POST /signup", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, user.Username, response.Username)
		assert.Equal(t, user.Email, response.Email)
		assert.Equal(t, user.Token, response.Token)
		assert.NotEqual(t, user.Password, response.Password) // it must not equal, because the password should have been encrypt
	})
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestUserSignInController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database
	password := PasswordEncrypt([]byte(user.Password)) // encrypt user password
	user.Password = password                           // assign encrypt password to user password
	database.UserSignUp(user)                          // insert dummy data into database
	// create dummy user login using email and password user
	user_login := models.User{
		Email:    user.Email,
		Password: "12345",
	}
	body, _ := json.Marshal(user_login) // input data using request body
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	// setting request url, must match with controller request
	context := e.NewContext(req, res)
	context.SetPath("/signin")
	// testing controller
	UserSignInController(context)
	// unmarshal response into object struct
	type Response struct {
		models.User
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("POST /sigin", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, user.Username, response.Username)
		assert.Equal(t, user.Email, response.Email)
		assert.NotEqual(t, "", response.Token) // it must not equal, because login is creating token, and before login the token is null
	})
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestUserSignOutController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database and create user token for authorization
	database.UserSignUp(user)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// setting controller
	e := echo.New()
	req := httptest.NewRequest(http.MethodPut, "/", nil)
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/signout/:user:id")
	context.SetParamNames("user_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(UserSignOutControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.User
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("PUT /signout/:user:id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, user.Username, response.Username)
		assert.NotEqual(t, user.Token, response.Token) // it must not equal, because signout process will make token null again
	})
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestUpdateUserController(t *testing.T) {
	// create database connection
	db, err := config.ConfigTest()
	if err != nil {
		t.Error(err)
	}
	// database migration
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
	db.AutoMigrate(&models.User{})          // create new table
	// inject user dummy data into database and create user token for authorization
	database.UserSignUp(user)
	// creat JWT token for authorization
	token, _ := middlewares.CreateToken(int(user.ID))
	// get user by id
	get_user, _ := database.GetUserById(1)
	// input updated data into request body
	updated_user := get_user
	updated_user.Username = "newUser"
	updated_user.Password = "0987654321"
	body, _ := json.Marshal(updated_user)
	// setting controller
	e := echo.New()
	// create testing request body
	req := httptest.NewRequest(http.MethodPut, "/", bytes.NewBuffer(body))
	req.Header.Set(echo.HeaderAuthorization, fmt.Sprintf("Bearer %v", token)) // input token for authorization user
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	/*
		record response code, if code = 200 it means success,
		if code = 4XX it means error from computer client,
		if code = 500 it means error from computer server
	*/
	res := httptest.NewRecorder()
	context := e.NewContext(req, res)
	// setting request url, must match with controller request
	context.SetPath("/users/:user_id")
	context.SetParamNames("user_id")
	context.SetParamValues("1")
	// testing controller using JWT Authorization
	middleware.JWT([]byte(constant.SECRET_JWT))(UpdateUserControllerTesting())(context)
	// unmarshal response into object struct
	type Response struct {
		models.User
	}
	var response Response
	req_body := res.Body.String()
	json.Unmarshal([]byte(req_body), &response)
	/*
		create a several test case to campare request to response,
		if equal test case will be passed
	*/
	t.Run("PUT /users/:user_id", func(t *testing.T) {
		assert.Equal(t, 200, res.Code)
		assert.Equal(t, updated_user.ID, response.ID)
		assert.Equal(t, updated_user.Username, response.Username)
		assert.Equal(t, updated_user.Email, response.Email)
		assert.NotEqual(t, updated_user.Password, response.Password) // it must not equal, because updated password should have been encrypt
		assert.NotEqual(t, get_user.Password, response.Password)     // it must not equal, because password had already updated
	})
	db.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}
