package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
)

func CreateFinancing(financing models.Financing) (models.OutputFinancing, error) {
	var output models.OutputFinancing
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&financing).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputFinancing{
		Name:  financing.Name,
		Count: financing.Count,
		Sub:   financing.Sub,
	}
	return output, nil
}

func GetAllFinancing() ([]models.OutputFinancing, error) {
	var financing []models.Financing
	var financing_output []models.OutputFinancing
	/*
		This is for finding and showing all of data from database, it is like query
		SELECT * FROM table_name;
	*/
	if err := config.DB.Find(&financing).Error; err != nil {
		return financing_output, err
	}
	//  create object for output
	for i := 0; i < len(financing); i++ {
		arr_financing := models.OutputFinancing{
			Name:  financing[i].Name,
			Count: financing[i].Count,
			Sub:   financing[i].Sub,
		}
		financing_output = append(financing_output, arr_financing)
	}
	return financing_output, nil
}

func GetFinancingByName(name string) ([]models.OutputFinancing, error) {
	var financing []models.Financing
	var financing_output []models.OutputFinancing
	/*
		This is for finding the specific data that using filter by name, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + name + "%"
	if err := config.DB.Find(&financing, "name LIKE ?", search).Error; err != nil {
		return financing_output, err
	}
	//  create object for output
	for i := 0; i < len(financing); i++ {
		financing := models.OutputFinancing{
			Name:  financing[i].Name,
			Count: financing[i].Count,
			Sub:   financing[i].Sub,
		}
		financing_output = append(financing_output, financing)
	}
	return financing_output, nil
}

func GetFinancingBySub(input string) ([]models.OutputFinancing, error) {
	var financing []models.Financing
	var financing_output []models.OutputFinancing
	/*
		This is for finding the specific data that using filter by sub, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + input + "%"
	if err := config.DB.Find(&financing, "sub LIKE ?", search).Error; err != nil {
		return financing_output, err
	}
	for i := 0; i < len(financing); i++ {
		financing := models.OutputFinancing{
			Name:  financing[i].Name,
			Count: financing[i].Count,
			Sub:   financing[i].Sub,
		}
		financing_output = append(financing_output, financing)
	}
	return financing_output, nil
}

func GetFinancingById(id int) (models.Financing, error) {
	var financing models.Financing
	/*
		This if for finding the specific data that using filter by id, it is like query
		SELECT * FROM table_name WHERE column_name = value ORDER BY id LIMIT 1;
	*/
	if err := config.DB.Where("id=?", id).First(&financing).Error; err != nil {
		return financing, err
	}
	return financing, nil
}

func UpdateFinancing(financing models.Financing) (models.OutputFinancing, error) {
	var output models.OutputFinancing
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&financing).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputFinancing{
		Name:  financing.Name,
		Count: financing.Count,
		Sub:   financing.Sub,
	}
	return output, nil
}

func DeleteFinancing(id int) (models.OutputFinancing, error) {
	var financing models.Financing
	var output models.OutputFinancing
	/*
		This is for finding the specific data that using filter by id, it is like query
		SELECT table_nameM table_name WHERE column_name = value;
	*/
	if err := config.DB.Find(&financing, "id=?", id).Error; err != nil {
		return output, err
	}
	/*
		This is for soft deleteing the specific data that using filter by id, it is like query
		UPDATE table_name SET deleted_at="...." WHERE id = ...;
	*/
	if err := config.DB.Delete(&financing, "id=?", id).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputFinancing{
		Name:  financing.Name,
		Count: financing.Count,
		Sub:   financing.Sub,
	}
	return output, nil
}
