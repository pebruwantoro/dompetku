package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	mockDBConventionalOSF = models.ConventionalOSF{
		ID:        1,
		Name:      "OSF1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBConventionalOSF1 = models.ConventionalOSF{
		ID:        1,
		Name:      "OSF1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBConventionalOSF2 = models.ConventionalOSF{
		ID:        2,
		Name:      "OSF2",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateConventionalOSF(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	osf, err := CreateConventionalOSF(mockDBConventionalOSF)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalOSF.Name, osf.Name)
		assert.Equal(t, mockDBConventionalOSF.Amount, osf.Amount)
		assert.Equal(t, mockDBConventionalOSF.Tenor, osf.Tenor)
		assert.Equal(t, mockDBConventionalOSF.Grade, osf.Grade)
		assert.Equal(t, mockDBConventionalOSF.Rate, osf.Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}

func TestGetAllConventionalOSF(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	CreateConventionalOSF(mockDBConventionalOSF1)
	CreateConventionalOSF(mockDBConventionalOSF2)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	osf, err := GetAllConventionalOSF()
	var osf_output []models.OutputConventionalOSF
	for i := 0; i < len(osf); i++ {
		arr_osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, arr_osf)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalOSF1.Name, osf_output[0].Name)
		assert.Equal(t, mockDBConventionalOSF1.Amount, osf_output[0].Amount)
		assert.Equal(t, mockDBConventionalOSF1.Tenor, osf_output[0].Tenor)
		assert.Equal(t, mockDBConventionalOSF1.Grade, osf_output[0].Grade)
		assert.Equal(t, mockDBConventionalOSF1.Rate, osf_output[0].Rate)
		assert.Equal(t, mockDBConventionalOSF2.Name, osf_output[1].Name)
		assert.Equal(t, mockDBConventionalOSF2.Amount, osf_output[1].Amount)
		assert.Equal(t, mockDBConventionalOSF2.Tenor, osf_output[1].Tenor)
		assert.Equal(t, mockDBConventionalOSF2.Grade, osf_output[1].Grade)
		assert.Equal(t, mockDBConventionalOSF2.Rate, osf_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}

func TestGetConventionalOSFByName(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	CreateConventionalOSF(mockDBConventionalOSF1)
	CreateConventionalOSF(mockDBConventionalOSF2)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	name := "OSF"
	osf, err := GetConventionalOSFByName(name)
	var osf_output []models.OutputConventionalOSF
	for i := 0; i < len(osf); i++ {
		arr_osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, arr_osf)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalOSF1.Name, osf_output[0].Name)
		assert.Equal(t, mockDBConventionalOSF1.Amount, osf_output[0].Amount)
		assert.Equal(t, mockDBConventionalOSF1.Tenor, osf_output[0].Tenor)
		assert.Equal(t, mockDBConventionalOSF1.Grade, osf_output[0].Grade)
		assert.Equal(t, mockDBConventionalOSF1.Rate, osf_output[0].Rate)
		assert.Equal(t, mockDBConventionalOSF2.Name, osf_output[1].Name)
		assert.Equal(t, mockDBConventionalOSF2.Amount, osf_output[1].Amount)
		assert.Equal(t, mockDBConventionalOSF2.Tenor, osf_output[1].Tenor)
		assert.Equal(t, mockDBConventionalOSF2.Grade, osf_output[1].Grade)
		assert.Equal(t, mockDBConventionalOSF2.Rate, osf_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}

func TestGetConventionalOSFByTenor(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	CreateConventionalOSF(mockDBConventionalOSF1)
	CreateConventionalOSF(mockDBConventionalOSF2)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	tenor := 12
	osf, err := GetConventionalOSFByTenor(tenor)
	var osf_output []models.OutputConventionalOSF
	for i := 0; i < len(osf); i++ {
		arr_osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, arr_osf)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalOSF1.Name, osf_output[0].Name)
		assert.Equal(t, mockDBConventionalOSF1.Amount, osf_output[0].Amount)
		assert.Equal(t, mockDBConventionalOSF1.Tenor, osf_output[0].Tenor)
		assert.Equal(t, mockDBConventionalOSF1.Grade, osf_output[0].Grade)
		assert.Equal(t, mockDBConventionalOSF1.Rate, osf_output[0].Rate)
		assert.Equal(t, mockDBConventionalOSF2.Name, osf_output[1].Name)
		assert.Equal(t, mockDBConventionalOSF2.Amount, osf_output[1].Amount)
		assert.Equal(t, mockDBConventionalOSF2.Tenor, osf_output[1].Tenor)
		assert.Equal(t, mockDBConventionalOSF2.Grade, osf_output[1].Grade)
		assert.Equal(t, mockDBConventionalOSF2.Rate, osf_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}

func TestGetConventionalOSFByGrade(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	CreateConventionalOSF(mockDBConventionalOSF1)
	CreateConventionalOSF(mockDBConventionalOSF2)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	grade := "A"
	osf, err := GetConventionalOSFByGrade(grade)
	var osf_output []models.OutputConventionalOSF
	for i := 0; i < len(osf); i++ {
		arr_osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, arr_osf)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalOSF1.Name, osf_output[0].Name)
		assert.Equal(t, mockDBConventionalOSF1.Amount, osf_output[0].Amount)
		assert.Equal(t, mockDBConventionalOSF1.Tenor, osf_output[0].Tenor)
		assert.Equal(t, mockDBConventionalOSF1.Grade, osf_output[0].Grade)
		assert.Equal(t, mockDBConventionalOSF1.Rate, osf_output[0].Rate)
		assert.Equal(t, mockDBConventionalOSF2.Name, osf_output[1].Name)
		assert.Equal(t, mockDBConventionalOSF2.Amount, osf_output[1].Amount)
		assert.Equal(t, mockDBConventionalOSF2.Tenor, osf_output[1].Tenor)
		assert.Equal(t, mockDBConventionalOSF2.Grade, osf_output[1].Grade)
		assert.Equal(t, mockDBConventionalOSF2.Rate, osf_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}

func TestGetConventionalOSFByRate(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	CreateConventionalOSF(mockDBConventionalOSF1)
	CreateConventionalOSF(mockDBConventionalOSF2)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	rate := 3
	osf, err := GetConventionalOSFByRate(rate)
	var osf_output []models.OutputConventionalOSF
	for i := 0; i < len(osf); i++ {
		arr_osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, arr_osf)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalOSF1.Name, osf_output[0].Name)
		assert.Equal(t, mockDBConventionalOSF1.Amount, osf_output[0].Amount)
		assert.Equal(t, mockDBConventionalOSF1.Tenor, osf_output[0].Tenor)
		assert.Equal(t, mockDBConventionalOSF1.Grade, osf_output[0].Grade)
		assert.Equal(t, mockDBConventionalOSF1.Rate, osf_output[0].Rate)
		assert.Equal(t, mockDBConventionalOSF2.Name, osf_output[1].Name)
		assert.Equal(t, mockDBConventionalOSF2.Amount, osf_output[1].Amount)
		assert.Equal(t, mockDBConventionalOSF2.Tenor, osf_output[1].Tenor)
		assert.Equal(t, mockDBConventionalOSF2.Grade, osf_output[1].Grade)
		assert.Equal(t, mockDBConventionalOSF2.Rate, osf_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}

func TestGetConventionalOSFById(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	CreateConventionalOSF(mockDBConventionalOSF1)
	CreateConventionalOSF(mockDBConventionalOSF2)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	id := 1
	osf, err := GetConventionalOSFById(id)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalOSF1.Name, osf.Name)
		assert.Equal(t, mockDBConventionalOSF1.Amount, osf.Amount)
		assert.Equal(t, mockDBConventionalOSF1.Tenor, osf.Tenor)
		assert.Equal(t, mockDBConventionalOSF1.Grade, osf.Grade)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}

func TestUpdateConventionalOSF(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	CreateConventionalOSF(mockDBConventionalOSF1)
	CreateConventionalOSF(mockDBConventionalOSF2)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	id := 1
	get_osf, _ := GetConventionalOSFById(id)
	get_osf.Name = "OSF Baru"
	get_osf.Amount = 100000000
	get_osf.Tenor = 12
	get_osf.Grade = "E"
	updated_osf, err := UpdateConventionalOSF(get_osf)
	if assert.NoError(t, err) {
		assert.Equal(t, updated_osf.Name, get_osf.Name)
		assert.Equal(t, updated_osf.Amount, get_osf.Amount)
		assert.Equal(t, updated_osf.Tenor, get_osf.Tenor)
		assert.Equal(t, updated_osf.Grade, get_osf.Grade)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}

func TestDeleteConventionalOSF(t *testing.T) {
	config.InitDbTest()                                         // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalOSF{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalOSF{}) // create table into database
	// inject ConventionalOSF data from MockDBConventionalOSF into ConventionalOSF's table
	CreateConventionalOSF(mockDBConventionalOSF1)
	CreateConventionalOSF(mockDBConventionalOSF2)
	// check and test ConventionalOSF data, if data injection exist in ConventionalOSF's table database, test will be pass
	id := 2
	deleted_osf, err := DeleteConventionalOSF(id)
	if assert.NoError(t, err) {
		assert.NotEqual(t, mockDBConventionalOSF1, deleted_osf)
		assert.NotEqual(t, mockDBConventionalOSF2, deleted_osf)
	}
	config.DB.Migrator().DropTable(&models.ConventionalOSF{}) // Delete the table to make empty table
}
