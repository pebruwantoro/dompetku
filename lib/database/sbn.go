package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"strconv"
)

func CreateSBN(sbn models.SBN) (models.OutputSBN, error) {
	var output models.OutputSBN
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&sbn).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputSBN{
		Name:   sbn.Name,
		Amount: sbn.Amount,
		Tenor:  sbn.Tenor,
		Rate:   sbn.Rate,
		Type:   sbn.Type,
	}
	return output, nil
}

func GetAllSBN() ([]models.OutputSBN, error) {
	var sbn []models.SBN
	var sbn_output []models.OutputSBN
	/*
		This is for finding and showing all of data from database, it is like query
		SELECT * FROM table_name;
	*/
	if err := config.DB.Find(&sbn).Error; err != nil {
		return sbn_output, err
	}
	//  create object for output
	for i := 0; i < len(sbn); i++ {
		arr_sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, arr_sbn)
	}
	return sbn_output, nil
}

func GetSBNByName(name string) ([]models.OutputSBN, error) {
	var sbn []models.SBN
	var sbn_output []models.OutputSBN
	/*
		This is for finding the specific data that using filter by name, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + name + "%"
	if err := config.DB.Find(&sbn, "name LIKE ?", search).Error; err != nil {
		return sbn_output, err
	}
	//  create object for output
	for i := 0; i < len(sbn); i++ {
		sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, sbn)
	}
	return sbn_output, nil
}

func GetSBNByTenor(tenor int) ([]models.OutputSBN, error) {
	var sbn []models.SBN
	var sbn_output []models.OutputSBN
	tenor_str := strconv.Itoa(tenor)
	/*
		This is for finding the specific data that using filter by tenor, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + tenor_str + "%"
	if err := config.DB.Find(&sbn, "tenor LIKE ?", search).Error; err != nil {
		return sbn_output, err
	}
	for i := 0; i < len(sbn); i++ {
		sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, sbn)
	}
	return sbn_output, nil
}

func GetSBNByRate(rate int) ([]models.OutputSBN, error) {
	var sbn []models.SBN
	var sbn_output []models.OutputSBN
	rate_str := strconv.Itoa(rate)
	/*
		This is for finding the specific data that using filter by rate, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + rate_str + "%"
	if err := config.DB.Find(&sbn, "rate LIKE ?", search).Error; err != nil {
		return sbn_output, err
	}
	//  create object for output
	for i := 0; i < len(sbn); i++ {
		sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, sbn)
	}
	return sbn_output, nil
}

func GetSBNByType(input string) ([]models.OutputSBN, error) {
	var sbn []models.SBN
	var sbn_output []models.OutputSBN
	/*
		This is for finding the specific data that using filter by grade, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + input + "%"
	if err := config.DB.Find(&sbn, "type LIKE ?", search).Error; err != nil {
		return sbn_output, err
	}
	//  create object for output
	for i := 0; i < len(sbn); i++ {
		sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, sbn)
	}
	return sbn_output, nil
}

func GetSBNById(id int) (models.SBN, error) {
	var sbn models.SBN
	/*
		This if for finding the specific data that using filter by id, it is like query
		SELECT * FROM table_name WHERE column_name = value ORDER BY id LIMIT 1;
	*/
	if err := config.DB.Where("id=?", id).First(&sbn).Error; err != nil {
		return sbn, err
	}
	return sbn, nil
}

func UpdateSBN(sbn models.SBN) (models.OutputSBN, error) {
	var output models.OutputSBN
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&sbn).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputSBN{
		Name:   sbn.Name,
		Amount: sbn.Amount,
		Tenor:  sbn.Tenor,
		Rate:   sbn.Rate,
		Type:   sbn.Type,
	}
	return output, nil
}

func DeleteSBN(id int) (models.OutputSBN, error) {
	var sbn models.SBN
	var output models.OutputSBN
	/*
		This is for finding the specific data that using filter by id, it is like query
		SELECT table_nameM table_name WHERE column_name = value;
	*/
	if err := config.DB.Find(&sbn, "id=?", id).Error; err != nil {
		return output, err
	}
	/*
		This is for soft deleteing the specific data that using filter by id, it is like query
		UPDATE table_name SET deleted_at="...." WHERE id = ...;
	*/
	if err := config.DB.Delete(&sbn, "id=?", id).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputSBN{
		Name:   sbn.Name,
		Amount: sbn.Amount,
		Tenor:  sbn.Tenor,
		Rate:   sbn.Rate,
		Type:   sbn.Type,
	}
	return output, nil
}
