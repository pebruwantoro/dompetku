package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	mockDBProductiveInvoice = models.ProductiveInvoice{
		ID:        1,
		Name:      "Invoice1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBProductiveInvoice1 = models.ProductiveInvoice{
		ID:        1,
		Name:      "Invoice1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBProductiveInvoice2 = models.ProductiveInvoice{
		ID:        2,
		Name:      "Invoice2",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateProductiveInvoice(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	prod_invoice, err := CreateProductiveInvoice(mockDBProductiveInvoice)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBProductiveInvoice.Name, prod_invoice.Name)
		assert.Equal(t, mockDBProductiveInvoice.Amount, prod_invoice.Amount)
		assert.Equal(t, mockDBProductiveInvoice.Tenor, prod_invoice.Tenor)
		assert.Equal(t, mockDBProductiveInvoice.Grade, prod_invoice.Grade)
		assert.Equal(t, mockDBProductiveInvoice.Rate, prod_invoice.Rate)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}

func TestGetAllProductiveInvoices(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	CreateProductiveInvoice(mockDBProductiveInvoice1)
	CreateProductiveInvoice(mockDBProductiveInvoice2)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	prod_invoices, err := GetAllProductiveInvoices()
	var prod_invoices_output []models.OutputProductiveInvoice
	for i := 0; i < len(prod_invoices); i++ {
		arr_prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoices[i].Name,
			Amount: prod_invoices[i].Amount,
			Tenor:  prod_invoices[i].Tenor,
			Grade:  prod_invoices[i].Grade,
			Rate:   prod_invoices[i].Rate,
		}
		prod_invoices_output = append(prod_invoices_output, arr_prod_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBProductiveInvoice1.Name, prod_invoices_output[0].Name)
		assert.Equal(t, mockDBProductiveInvoice1.Amount, prod_invoices_output[0].Amount)
		assert.Equal(t, mockDBProductiveInvoice1.Tenor, prod_invoices_output[0].Tenor)
		assert.Equal(t, mockDBProductiveInvoice1.Grade, prod_invoices_output[0].Grade)
		assert.Equal(t, mockDBProductiveInvoice1.Rate, prod_invoices_output[0].Rate)
		assert.Equal(t, mockDBProductiveInvoice2.Name, prod_invoices_output[1].Name)
		assert.Equal(t, mockDBProductiveInvoice2.Amount, prod_invoices_output[1].Amount)
		assert.Equal(t, mockDBProductiveInvoice2.Tenor, prod_invoices_output[1].Tenor)
		assert.Equal(t, mockDBProductiveInvoice2.Grade, prod_invoices_output[1].Grade)
		assert.Equal(t, mockDBProductiveInvoice2.Rate, prod_invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}

func TestGetProductiveInvoiceByName(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	CreateProductiveInvoice(mockDBProductiveInvoice1)
	CreateProductiveInvoice(mockDBProductiveInvoice2)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	name := "invoice"
	prod_invoices, err := GetProductiveInvoiceByName(name)
	var prod_invoices_output []models.OutputProductiveInvoice
	for i := 0; i < len(prod_invoices); i++ {
		arr_prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoices[i].Name,
			Amount: prod_invoices[i].Amount,
			Tenor:  prod_invoices[i].Tenor,
			Grade:  prod_invoices[i].Grade,
			Rate:   prod_invoices[i].Rate,
		}
		prod_invoices_output = append(prod_invoices_output, arr_prod_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBProductiveInvoice1.Name, prod_invoices_output[0].Name)
		assert.Equal(t, mockDBProductiveInvoice1.Amount, prod_invoices_output[0].Amount)
		assert.Equal(t, mockDBProductiveInvoice1.Tenor, prod_invoices_output[0].Tenor)
		assert.Equal(t, mockDBProductiveInvoice1.Grade, prod_invoices_output[0].Grade)
		assert.Equal(t, mockDBProductiveInvoice1.Rate, prod_invoices_output[0].Rate)
		assert.Equal(t, mockDBProductiveInvoice2.Name, prod_invoices_output[1].Name)
		assert.Equal(t, mockDBProductiveInvoice2.Amount, prod_invoices_output[1].Amount)
		assert.Equal(t, mockDBProductiveInvoice2.Tenor, prod_invoices_output[1].Tenor)
		assert.Equal(t, mockDBProductiveInvoice2.Grade, prod_invoices_output[1].Grade)
		assert.Equal(t, mockDBProductiveInvoice2.Rate, prod_invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}

func TestGetProductiveInvoiceByTenor(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	CreateProductiveInvoice(mockDBProductiveInvoice1)
	CreateProductiveInvoice(mockDBProductiveInvoice2)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	tenor := 12
	prod_invoices, err := GetProductiveInvoiceByTenor(tenor)
	var prod_invoices_output []models.OutputProductiveInvoice
	for i := 0; i < len(prod_invoices); i++ {
		arr_prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoices[i].Name,
			Amount: prod_invoices[i].Amount,
			Tenor:  prod_invoices[i].Tenor,
			Grade:  prod_invoices[i].Grade,
			Rate:   prod_invoices[i].Rate,
		}
		prod_invoices_output = append(prod_invoices_output, arr_prod_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBProductiveInvoice1.Name, prod_invoices_output[0].Name)
		assert.Equal(t, mockDBProductiveInvoice1.Amount, prod_invoices_output[0].Amount)
		assert.Equal(t, mockDBProductiveInvoice1.Tenor, prod_invoices_output[0].Tenor)
		assert.Equal(t, mockDBProductiveInvoice1.Grade, prod_invoices_output[0].Grade)
		assert.Equal(t, mockDBProductiveInvoice1.Rate, prod_invoices_output[0].Rate)
		assert.Equal(t, mockDBProductiveInvoice2.Name, prod_invoices_output[1].Name)
		assert.Equal(t, mockDBProductiveInvoice2.Amount, prod_invoices_output[1].Amount)
		assert.Equal(t, mockDBProductiveInvoice2.Tenor, prod_invoices_output[1].Tenor)
		assert.Equal(t, mockDBProductiveInvoice2.Grade, prod_invoices_output[1].Grade)
		assert.Equal(t, mockDBProductiveInvoice2.Rate, prod_invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}

func TestGetProductiveInvoiceByGrade(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	CreateProductiveInvoice(mockDBProductiveInvoice1)
	CreateProductiveInvoice(mockDBProductiveInvoice2)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	grade := "A"
	invoices, err := GetProductiveInvoiceByGrade(grade)
	var invoices_output []models.OutputProductiveInvoice
	for i := 0; i < len(invoices); i++ {
		arr_invoices := models.OutputProductiveInvoice{
			Name:   invoices[i].Name,
			Amount: invoices[i].Amount,
			Tenor:  invoices[i].Tenor,
			Grade:  invoices[i].Grade,
			Rate:   invoices[i].Rate,
		}
		invoices_output = append(invoices_output, arr_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBProductiveInvoice1.Name, invoices_output[0].Name)
		assert.Equal(t, mockDBProductiveInvoice1.Amount, invoices_output[0].Amount)
		assert.Equal(t, mockDBProductiveInvoice1.Tenor, invoices_output[0].Tenor)
		assert.Equal(t, mockDBProductiveInvoice1.Grade, invoices_output[0].Grade)
		assert.Equal(t, mockDBProductiveInvoice1.Rate, invoices_output[0].Rate)
		assert.Equal(t, mockDBProductiveInvoice2.Name, invoices_output[1].Name)
		assert.Equal(t, mockDBProductiveInvoice2.Amount, invoices_output[1].Amount)
		assert.Equal(t, mockDBProductiveInvoice2.Tenor, invoices_output[1].Tenor)
		assert.Equal(t, mockDBProductiveInvoice2.Grade, invoices_output[1].Grade)
		assert.Equal(t, mockDBProductiveInvoice2.Rate, invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}

func TestGetProductiveInvoiceByRate(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	CreateProductiveInvoice(mockDBProductiveInvoice1)
	CreateProductiveInvoice(mockDBProductiveInvoice2)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	rate := 3
	prod_invoices, err := GetProductiveInvoiceByRate(rate)
	var prod_invoices_output []models.OutputProductiveInvoice
	for i := 0; i < len(prod_invoices); i++ {
		arr_prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoices[i].Name,
			Amount: prod_invoices[i].Amount,
			Tenor:  prod_invoices[i].Tenor,
			Grade:  prod_invoices[i].Grade,
			Rate:   prod_invoices[i].Rate,
		}
		prod_invoices_output = append(prod_invoices_output, arr_prod_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBProductiveInvoice1.Name, prod_invoices_output[0].Name)
		assert.Equal(t, mockDBProductiveInvoice1.Amount, prod_invoices_output[0].Amount)
		assert.Equal(t, mockDBProductiveInvoice1.Tenor, prod_invoices_output[0].Tenor)
		assert.Equal(t, mockDBProductiveInvoice1.Grade, prod_invoices_output[0].Grade)
		assert.Equal(t, mockDBProductiveInvoice1.Rate, prod_invoices_output[0].Rate)
		assert.Equal(t, mockDBProductiveInvoice2.Name, prod_invoices_output[1].Name)
		assert.Equal(t, mockDBProductiveInvoice2.Amount, prod_invoices_output[1].Amount)
		assert.Equal(t, mockDBProductiveInvoice2.Tenor, prod_invoices_output[1].Tenor)
		assert.Equal(t, mockDBProductiveInvoice2.Grade, prod_invoices_output[1].Grade)
		assert.Equal(t, mockDBProductiveInvoice2.Rate, prod_invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}

func TestGetProductiveInvoiceById(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	CreateProductiveInvoice(mockDBProductiveInvoice1)
	CreateProductiveInvoice(mockDBProductiveInvoice2)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	id := 1
	invoice, err := GetProductiveInvoiceById(id)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBProductiveInvoice1.Name, invoice.Name)
		assert.Equal(t, mockDBProductiveInvoice1.Amount, invoice.Amount)
		assert.Equal(t, mockDBProductiveInvoice1.Tenor, invoice.Tenor)
		assert.Equal(t, mockDBProductiveInvoice1.Grade, invoice.Grade)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}

func TestUpdateProductiveInvoice(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	CreateProductiveInvoice(mockDBProductiveInvoice1)
	CreateProductiveInvoice(mockDBProductiveInvoice2)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	id := 1
	get_prod_invoice, _ := GetProductiveInvoiceById(id)
	get_prod_invoice.Name = "Invoice Baru"
	get_prod_invoice.Amount = 100000000
	get_prod_invoice.Tenor = 12
	get_prod_invoice.Grade = "E"
	updated_prod_invoice, err := UpdateProductiveInvoice(get_prod_invoice)
	if assert.NoError(t, err) {
		assert.Equal(t, updated_prod_invoice.Name, get_prod_invoice.Name)
		assert.Equal(t, updated_prod_invoice.Amount, get_prod_invoice.Amount)
		assert.Equal(t, updated_prod_invoice.Tenor, get_prod_invoice.Tenor)
		assert.Equal(t, updated_prod_invoice.Grade, get_prod_invoice.Grade)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}

func TestDeleteProductiveInvoice(t *testing.T) {
	config.InitDbTest()                                           // connect to database
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ProductiveInvoice{}) // create table into database
	// inject ProductiveInvoice data from MockDBProductiveInvoice into ProductiveInvoice's table
	CreateProductiveInvoice(mockDBProductiveInvoice1)
	CreateProductiveInvoice(mockDBProductiveInvoice2)
	// check and test ProductiveInvoice data, if data injection exist in ProductiveInvoice's table database, test will be pass
	id := 2
	deleted_prod_invoice, err := DeleteProductiveInvoice(id)
	if assert.NoError(t, err) {
		assert.NotEqual(t, mockDBProductiveInvoice1, deleted_prod_invoice)
		assert.NotEqual(t, mockDBProductiveInvoice2, deleted_prod_invoice)
	}
	config.DB.Migrator().DropTable(&models.ProductiveInvoice{}) // Delete the table to make empty table
}
