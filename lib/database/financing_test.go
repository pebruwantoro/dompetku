package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	mockDBFinancing = models.Financing{
		ID:        1,
		Name:      "OSF1",
		Count:     25,
		Sub:       "OSF",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBFinancing1 = models.Financing{
		ID:        1,
		Name:      "OSF1",
		Count:     12,
		Sub:       "OSF",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBFinancing2 = models.Financing{
		ID:        2,
		Name:      "OSF2",
		Count:     2,
		Sub:       "OSF",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateFinancing(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Financing{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Financing{}) // create table into database
	// inject Financing data from MockDBFinancing into Financing's table
	financing, err := CreateFinancing(mockDBFinancing)
	// check and test Financing data, if data injection exist in Financing's table database, test will be pass
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBFinancing.Name, financing.Name)
		assert.Equal(t, mockDBFinancing.Count, financing.Count)
		assert.Equal(t, mockDBFinancing.Sub, financing.Sub)
	}
	config.DB.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
}

func TestGetAllFinancing(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Financing{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Financing{}) // create table into database
	// inject Financing data from MockDBFinancing into Financing's table
	CreateFinancing(mockDBFinancing1)
	CreateFinancing(mockDBFinancing2)
	// check and test Financing data, if data injection exist in Financing's table database, test will be pass
	financing, err := GetAllFinancing()
	var financing_output []models.OutputFinancing
	for i := 0; i < len(financing); i++ {
		arr_financing := models.OutputFinancing{
			Name:  financing[i].Name,
			Count: financing[i].Count,
			Sub:   financing[i].Sub,
		}
		financing_output = append(financing_output, arr_financing)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBFinancing1.Name, financing_output[0].Name)
		assert.Equal(t, mockDBFinancing1.Count, financing_output[0].Count)
		assert.Equal(t, mockDBFinancing1.Sub, financing_output[0].Sub)
		assert.Equal(t, mockDBFinancing2.Name, financing_output[1].Name)
		assert.Equal(t, mockDBFinancing2.Count, financing_output[1].Count)
		assert.Equal(t, mockDBFinancing2.Sub, financing_output[1].Sub)
	}
	config.DB.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
}

func TestGetFinancingByName(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Financing{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Financing{}) // create table into database
	// inject Financing data from MockDBFinancing into Financing's table
	CreateFinancing(mockDBFinancing1)
	CreateFinancing(mockDBFinancing2)
	// check and test Financing data, if data injection exist in Financing's table database, test will be pass
	name := "OSF"
	financing, err := GetFinancingByName(name)
	var financing_output []models.OutputFinancing
	for i := 0; i < len(financing); i++ {
		arr_financing := models.OutputFinancing{
			Name:  financing[i].Name,
			Count: financing[i].Count,
			Sub:   financing[i].Sub,
		}
		financing_output = append(financing_output, arr_financing)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBFinancing1.Name, financing_output[0].Name)
		assert.Equal(t, mockDBFinancing1.Count, financing_output[0].Count)
		assert.Equal(t, mockDBFinancing1.Sub, financing_output[0].Sub)
		assert.Equal(t, mockDBFinancing2.Name, financing_output[1].Name)
		assert.Equal(t, mockDBFinancing2.Count, financing_output[1].Count)
		assert.Equal(t, mockDBFinancing2.Sub, financing_output[1].Sub)
	}
	config.DB.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
}

func TestGetFinancingBySub(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Financing{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Financing{}) // create table into database
	// inject Financing data from MockDBFinancing into Financing's table
	CreateFinancing(mockDBFinancing1)
	CreateFinancing(mockDBFinancing2)
	// check and test Financing data, if data injection exist in Financing's table database, test will be pass
	sub := "OSF"
	financing, err := GetFinancingBySub(sub)
	var financing_output []models.OutputFinancing
	for i := 0; i < len(financing); i++ {
		arr_financing := models.OutputFinancing{
			Name:  financing[i].Name,
			Count: financing[i].Count,
			Sub:   financing[i].Sub,
		}
		financing_output = append(financing_output, arr_financing)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBFinancing1.Name, financing_output[0].Name)
		assert.Equal(t, mockDBFinancing1.Count, financing_output[0].Count)
		assert.Equal(t, mockDBFinancing1.Sub, financing_output[0].Sub)
		assert.Equal(t, mockDBFinancing2.Name, financing_output[1].Name)
		assert.Equal(t, mockDBFinancing2.Count, financing_output[1].Count)
		assert.Equal(t, mockDBFinancing2.Sub, financing_output[1].Sub)
	}
	config.DB.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
}

func TestGetFinancingById(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Financing{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Financing{}) // create table into database
	// inject Financing data from MockDBFinancing into Financing's table
	CreateFinancing(mockDBFinancing1)
	CreateFinancing(mockDBFinancing2)
	// check and test Financing data, if data injection exist in Financing's table database, test will be pass
	id := 1
	financing, err := GetFinancingById(id)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBFinancing1.Name, financing.Name)
		assert.Equal(t, mockDBFinancing1.Count, financing.Count)
		assert.Equal(t, mockDBFinancing1.Sub, financing.Sub)
	}
	config.DB.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
}

func TestUpdateFinancing(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Financing{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Financing{}) // create table into database
	// inject Financing data from MockDBFinancing into Financing's table
	CreateFinancing(mockDBFinancing1)
	CreateFinancing(mockDBFinancing2)
	// check and test Financing data, if data injection exist in Financing's table database, test will be pass
	id := 1
	get_financing, _ := GetFinancingById(id)
	get_financing.Name = "Financing Baru"
	get_financing.Count = 1
	get_financing.Sub = "Baru"
	updated_financing, err := UpdateFinancing(get_financing)
	if assert.NoError(t, err) {
		assert.Equal(t, get_financing.Name, updated_financing.Name)
		assert.Equal(t, get_financing.Count, updated_financing.Count)
		assert.Equal(t, get_financing.Sub, updated_financing.Sub)
	}
	config.DB.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
}

func TestDeleteFinancing(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Financing{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Financing{}) // create table into database
	// inject Financing data from MockDBFinancing into Financing's table
	CreateFinancing(mockDBFinancing1)
	CreateFinancing(mockDBFinancing2)
	// check and test Financing data, if data injection exist in Financing's table database, test will be pass
	id := 2
	deleted_financing, err := DeleteFinancing(id)
	if assert.NoError(t, err) {
		assert.NotEqual(t, mockDBFinancing1, deleted_financing)
		assert.NotEqual(t, mockDBFinancing2, deleted_financing)
	}
	config.DB.Migrator().DropTable(&models.Financing{}) // Delete the table to make empty table
}
