package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	mockDBSBN = models.SBN{
		ID:        1,
		Name:      "SBN1",
		Amount:    100000,
		Tenor:     12,
		Rate:      3,
		Type:      "Financing",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBSBN1 = models.SBN{
		ID:        1,
		Name:      "SBN1",
		Amount:    100000,
		Tenor:     12,
		Rate:      3,
		Type:      "Financing",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBSBN2 = models.SBN{
		ID:        2,
		Name:      "SBN2",
		Amount:    100000,
		Tenor:     12,
		Rate:      3,
		Type:      "Financing",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateSBN(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	sbn, err := CreateSBN(mockDBSBN)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBSBN.Name, sbn.Name)
		assert.Equal(t, mockDBSBN.Amount, sbn.Amount)
		assert.Equal(t, mockDBSBN.Tenor, sbn.Tenor)
		assert.Equal(t, mockDBSBN.Rate, sbn.Rate)
		assert.Equal(t, mockDBSBN.Type, sbn.Type)
	}
	config.DB.Migrator().DropTable(&models.SBN{}) // Delete the table to make empty table
}

func TestGetAllSBN(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	CreateSBN(mockDBSBN1)
	CreateSBN(mockDBSBN2)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	sbn, err := GetAllSBN()
	var sbn_output []models.OutputSBN
	for i := 0; i < len(sbn); i++ {
		arr_sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, arr_sbn)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBSBN1.Name, sbn_output[0].Name)
		assert.Equal(t, mockDBSBN1.Amount, sbn_output[0].Amount)
		assert.Equal(t, mockDBSBN1.Tenor, sbn_output[0].Tenor)
		assert.Equal(t, mockDBSBN1.Rate, sbn_output[0].Rate)
		assert.Equal(t, mockDBSBN1.Type, sbn_output[0].Type)
		assert.Equal(t, mockDBSBN2.Name, sbn_output[1].Name)
		assert.Equal(t, mockDBSBN2.Amount, sbn_output[1].Amount)
		assert.Equal(t, mockDBSBN2.Tenor, sbn_output[1].Tenor)
		assert.Equal(t, mockDBSBN2.Rate, sbn_output[1].Rate)
		assert.Equal(t, mockDBSBN2.Type, sbn_output[1].Type)
	}
	config.DB.Migrator().DropTable(&models.SBN{}) // Delete the table to make empty table
}

func TestGetSBNByName(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	CreateSBN(mockDBSBN1)
	CreateSBN(mockDBSBN2)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	name := "SBN"
	sbn, err := GetSBNByName(name)
	var sbn_output []models.OutputSBN
	for i := 0; i < len(sbn); i++ {
		arr_sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, arr_sbn)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBSBN1.Name, sbn_output[0].Name)
		assert.Equal(t, mockDBSBN1.Amount, sbn_output[0].Amount)
		assert.Equal(t, mockDBSBN1.Tenor, sbn_output[0].Tenor)
		assert.Equal(t, mockDBSBN1.Rate, sbn_output[0].Rate)
		assert.Equal(t, mockDBSBN1.Type, sbn_output[0].Type)
		assert.Equal(t, mockDBSBN2.Name, sbn_output[1].Name)
		assert.Equal(t, mockDBSBN2.Amount, sbn_output[1].Amount)
		assert.Equal(t, mockDBSBN2.Tenor, sbn_output[1].Tenor)
		assert.Equal(t, mockDBSBN2.Rate, sbn_output[1].Rate)
		assert.Equal(t, mockDBSBN2.Type, sbn_output[1].Type)
	}
	config.DB.Migrator().DropTable(&models.SBN{}) // Delete the table to make empty table
}

func TestGetSBNByTenor(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	CreateSBN(mockDBSBN1)
	CreateSBN(mockDBSBN2)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	tenor := 12
	sbn, err := GetSBNByTenor(tenor)
	var sbn_output []models.OutputSBN
	for i := 0; i < len(sbn); i++ {
		arr_sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, arr_sbn)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBSBN1.Name, sbn_output[0].Name)
		assert.Equal(t, mockDBSBN1.Amount, sbn_output[0].Amount)
		assert.Equal(t, mockDBSBN1.Tenor, sbn_output[0].Tenor)
		assert.Equal(t, mockDBSBN1.Rate, sbn_output[0].Rate)
		assert.Equal(t, mockDBSBN1.Type, sbn_output[0].Type)
		assert.Equal(t, mockDBSBN2.Name, sbn_output[1].Name)
		assert.Equal(t, mockDBSBN2.Amount, sbn_output[1].Amount)
		assert.Equal(t, mockDBSBN2.Tenor, sbn_output[1].Tenor)
		assert.Equal(t, mockDBSBN2.Rate, sbn_output[1].Rate)
		assert.Equal(t, mockDBSBN2.Type, sbn_output[1].Type)
	}
	config.DB.Migrator().DropTable(&models.SBN{}) // Delete the table to make empty table
}

func TestGetSBNByRate(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	CreateSBN(mockDBSBN1)
	CreateSBN(mockDBSBN2)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	rate := 3
	sbn, err := GetSBNByRate(rate)
	var sbn_output []models.OutputSBN
	for i := 0; i < len(sbn); i++ {
		arr_sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, arr_sbn)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBSBN1.Name, sbn_output[0].Name)
		assert.Equal(t, mockDBSBN1.Amount, sbn_output[0].Amount)
		assert.Equal(t, mockDBSBN1.Tenor, sbn_output[0].Tenor)
		assert.Equal(t, mockDBSBN1.Rate, sbn_output[0].Rate)
		assert.Equal(t, mockDBSBN1.Type, sbn_output[0].Type)
		assert.Equal(t, mockDBSBN2.Name, sbn_output[1].Name)
		assert.Equal(t, mockDBSBN2.Amount, sbn_output[1].Amount)
		assert.Equal(t, mockDBSBN2.Tenor, sbn_output[1].Tenor)
		assert.Equal(t, mockDBSBN2.Rate, sbn_output[1].Rate)
		assert.Equal(t, mockDBSBN2.Type, sbn_output[1].Type)
	}
	config.DB.Migrator().DropTable(&models.SBN{}) // Delete the table to make empty table
}

func TestGetSBNByType(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	CreateSBN(mockDBSBN1)
	CreateSBN(mockDBSBN2)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	input := "Financing"
	sbn, err := GetSBNByType(input)
	var sbn_output []models.OutputSBN
	for i := 0; i < len(sbn); i++ {
		arr_sbn := models.OutputSBN{
			Name:   sbn[i].Name,
			Amount: sbn[i].Amount,
			Tenor:  sbn[i].Tenor,
			Rate:   sbn[i].Rate,
			Type:   sbn[i].Type,
		}
		sbn_output = append(sbn_output, arr_sbn)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBSBN1.Name, sbn_output[0].Name)
		assert.Equal(t, mockDBSBN1.Amount, sbn_output[0].Amount)
		assert.Equal(t, mockDBSBN1.Tenor, sbn_output[0].Tenor)
		assert.Equal(t, mockDBSBN1.Rate, sbn_output[0].Rate)
		assert.Equal(t, mockDBSBN1.Type, sbn_output[0].Type)
		assert.Equal(t, mockDBSBN2.Name, sbn_output[1].Name)
		assert.Equal(t, mockDBSBN2.Amount, sbn_output[1].Amount)
		assert.Equal(t, mockDBSBN2.Tenor, sbn_output[1].Tenor)
		assert.Equal(t, mockDBSBN2.Rate, sbn_output[1].Rate)
		assert.Equal(t, mockDBSBN2.Type, sbn_output[1].Type)
	}
	config.DB.Migrator().DropTable(&models.SBN{}) // Delete the table to make empty table
}
func TestGetSBNById(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	CreateSBN(mockDBSBN1)
	CreateSBN(mockDBSBN2)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	id := 1
	invoice, err := GetSBNById(id)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBSBN1.Name, invoice.Name)
		assert.Equal(t, mockDBSBN1.Amount, invoice.Amount)
		assert.Equal(t, mockDBSBN1.Tenor, invoice.Tenor)
		assert.Equal(t, mockDBSBN1.Rate, invoice.Rate)
		assert.Equal(t, mockDBSBN1.Type, invoice.Type)
	}
}

func TestUpdateSBN(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	CreateSBN(mockDBSBN1)
	CreateSBN(mockDBSBN2)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	id := 1
	get_sbn, _ := GetSBNById(id)
	get_sbn.Name = "Financing Baru"
	get_sbn.Amount = 100000000
	get_sbn.Tenor = 9
	get_sbn.Rate = 4
	get_sbn.Type = "E"
	updated_sbn, err := UpdateSBN(get_sbn)
	if assert.NoError(t, err) {
		assert.Equal(t, updated_sbn.Name, get_sbn.Name)
		assert.Equal(t, updated_sbn.Amount, get_sbn.Amount)
		assert.Equal(t, updated_sbn.Tenor, get_sbn.Tenor)
		assert.Equal(t, updated_sbn.Type, get_sbn.Type)
	}
	config.DB.Migrator().DropTable(&models.SBN{}) // Delete the table to make empty table
}

func TestDeleteSBN(t *testing.T) {
	config.InitDbTest()                             // connect to database
	config.DB.Migrator().DropTable(&models.SBN{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.SBN{}) // create table into database
	// inject SBN data from MockDBSBN into SBN's table
	CreateSBN(mockDBSBN1)
	CreateSBN(mockDBSBN2)
	// check and test SBN data, if data injection exist in SBN's table database, test will be pass
	id := 2
	deleted_sbn, err := DeleteSBN(id)
	if assert.NoError(t, err) {
		assert.NotEqual(t, mockDBSBN1, deleted_sbn)
		assert.NotEqual(t, mockDBSBN2, deleted_sbn)
	}
	config.DB.Migrator().DropTable(&models.SBN{}) // Delete the table to make empty table
}
