package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"strconv"
)

func CreateConventionalOSF(osf models.ConventionalOSF) (models.OutputConventionalOSF, error) {
	var output models.OutputConventionalOSF
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&osf).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputConventionalOSF{
		Name:   osf.Name,
		Amount: osf.Amount,
		Tenor:  osf.Tenor,
		Grade:  osf.Grade,
		Rate:   osf.Rate,
	}
	return output, nil
}
func GetAllConventionalOSF() ([]models.OutputConventionalOSF, error) {
	var osf []models.ConventionalOSF
	var osf_output []models.OutputConventionalOSF
	/*
		This is for finding and showing all of data from database, it is like query
		SELECT * FROM table_name;
	*/
	if err := config.DB.Find(&osf).Error; err != nil {
		return osf_output, err
	}
	//  create object for output
	for i := 0; i < len(osf); i++ {
		arr_osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, arr_osf)
	}
	return osf_output, nil
}

func GetConventionalOSFByName(name string) ([]models.OutputConventionalOSF, error) {
	var osf []models.ConventionalOSF
	var osf_output []models.OutputConventionalOSF
	/*
		This is for finding the specific data that using filter by name, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + name + "%"
	if err := config.DB.Find(&osf, "name LIKE ?", search).Error; err != nil {
		return osf_output, err
	}
	//  create object for output
	for i := 0; i < len(osf); i++ {
		osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, osf)
	}
	return osf_output, nil
}

func GetConventionalOSFByTenor(tenor int) ([]models.OutputConventionalOSF, error) {
	var osf []models.ConventionalOSF
	var osf_output []models.OutputConventionalOSF
	tenor_str := strconv.Itoa(tenor)
	/*
		This is for finding the specific data that using filter by tenor, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + tenor_str + "%"
	if err := config.DB.Find(&osf, "tenor LIKE ?", search).Error; err != nil {
		return osf_output, err
	}
	//  create object for output
	for i := 0; i < len(osf); i++ {
		osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, osf)
	}
	return osf_output, nil
}

func GetConventionalOSFByGrade(grade string) ([]models.OutputConventionalOSF, error) {
	var osf []models.ConventionalOSF
	var osf_output []models.OutputConventionalOSF
	/*
		This is for finding the specific data that using filter by grade, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + grade + "%"
	if err := config.DB.Find(&osf, "grade LIKE ?", search).Error; err != nil {
		return osf_output, err
	}
	//  create object for output
	for i := 0; i < len(osf); i++ {
		osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, osf)
	}
	return osf_output, nil
}

func GetConventionalOSFByRate(rate int) ([]models.OutputConventionalOSF, error) {
	var osf []models.ConventionalOSF
	var osf_output []models.OutputConventionalOSF
	rate_str := strconv.Itoa(rate)
	/*
		This is for finding the specific data that using filter by rate, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + rate_str + "%"
	if err := config.DB.Find(&osf, "rate LIKE ?", search).Error; err != nil {
		return osf_output, err
	}
	//  create object for output
	for i := 0; i < len(osf); i++ {
		osf := models.OutputConventionalOSF{
			Name:   osf[i].Name,
			Amount: osf[i].Amount,
			Tenor:  osf[i].Tenor,
			Grade:  osf[i].Grade,
			Rate:   osf[i].Rate,
		}
		osf_output = append(osf_output, osf)
	}
	return osf_output, nil
}
func GetConventionalOSFById(id int) (models.ConventionalOSF, error) {
	var osf models.ConventionalOSF
	/*
		This if for finding the specific data that using filter by id, it is like query
		SELECT * FROM table_name WHERE column_name = value ORDER BY id LIMIT 1;
	*/
	if err := config.DB.Where("id=?", id).First(&osf).Error; err != nil {
		return osf, err
	}
	return osf, nil
}
func UpdateConventionalOSF(osf models.ConventionalOSF) (models.OutputConventionalOSF, error) {
	var output models.OutputConventionalOSF
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&osf).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputConventionalOSF{
		Name:   osf.Name,
		Amount: osf.Amount,
		Tenor:  osf.Tenor,
		Grade:  osf.Grade,
		Rate:   osf.Rate,
	}
	return output, nil
}

func DeleteConventionalOSF(id int) (models.OutputConventionalOSF, error) {
	var osf models.ConventionalOSF
	var output models.OutputConventionalOSF
	/*
		This is for finding the specific data that using filter by id, it is like query
		SELECT table_nameM table_name WHERE column_name = value;
	*/
	if err := config.DB.Find(&osf, "id=?", id).Error; err != nil {
		return output, err
	}
	/*
		This is for soft deleteing the specific data that using filter by id, it is like query
		UPDATE table_name SET deleted_at="...." WHERE id = ...;
	*/
	if err := config.DB.Delete(&osf, "id=?", id).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputConventionalOSF{
		Name:   osf.Name,
		Amount: osf.Amount,
		Tenor:  osf.Tenor,
		Grade:  osf.Grade,
		Rate:   osf.Rate,
	}
	return output, nil
}
