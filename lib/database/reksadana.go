package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"strconv"
)

func CreateReksadana(reksadana models.Reksadana) (models.OutputReksadana, error) {
	var output models.OutputReksadana
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&reksadana).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputReksadana{
		Name:   reksadana.Name,
		Amount: reksadana.Amount,
		Return: reksadana.Reksadana_Return,
	}
	return output, nil
}

func GetAllReksadana() ([]models.OutputReksadana, error) {
	var reksadana []models.Reksadana
	var reksadana_output []models.OutputReksadana
	/*
		This is for finding and showing all of data from database, it is like query
		SELECT * FROM table_name;
	*/
	if err := config.DB.Find(&reksadana).Error; err != nil {
		return reksadana_output, err
	}
	//  create object for output
	for i := 0; i < len(reksadana); i++ {
		arr_reksadana := models.OutputReksadana{
			Name:   reksadana[i].Name,
			Amount: reksadana[i].Amount,
			Return: reksadana[i].Reksadana_Return,
		}
		reksadana_output = append(reksadana_output, arr_reksadana)
	}
	return reksadana_output, nil
}

func GetReksadanaByName(name string) ([]models.OutputReksadana, error) {
	var reksadana []models.Reksadana
	var reksadana_output []models.OutputReksadana
	/*
		This is for finding the specific data that using filter by name, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + name + "%"
	if err := config.DB.Find(&reksadana, "name LIKE ?", search).Error; err != nil {
		return reksadana_output, err
	}
	//  create object for output
	for i := 0; i < len(reksadana); i++ {
		reksadana := models.OutputReksadana{
			Name:   reksadana[i].Name,
			Amount: reksadana[i].Amount,
			Return: reksadana[i].Reksadana_Return,
		}
		reksadana_output = append(reksadana_output, reksadana)
	}
	return reksadana_output, nil
}

func GetReksadanaByReturn(input int) ([]models.OutputReksadana, error) {
	var reksadana []models.Reksadana
	var reksadana_output []models.OutputReksadana
	input_str := strconv.Itoa(input)
	/*
		This is for finding the specific data that using filter by return, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + input_str + "%"
	if err := config.DB.Find(&reksadana, "reksadana_return LIKE ?", search).Error; err != nil {
		return reksadana_output, err
	}
	for i := 0; i < len(reksadana); i++ {
		reksadana := models.OutputReksadana{
			Name:   reksadana[i].Name,
			Amount: reksadana[i].Amount,
			Return: reksadana[i].Reksadana_Return,
		}
		reksadana_output = append(reksadana_output, reksadana)
	}
	return reksadana_output, nil
}

func GetReksadanaById(id int) (models.Reksadana, error) {
	var reksadana models.Reksadana
	/*
		This if for finding the specific data that using filter by id, it is like query
		SELECT * FROM table_name WHERE column_name = value ORDER BY id LIMIT 1;
	*/
	if err := config.DB.Where("id=?", id).First(&reksadana).Error; err != nil {
		return reksadana, err
	}
	return reksadana, nil
}

func UpdateReksadana(reksadana models.Reksadana) (models.OutputReksadana, error) {
	var output models.OutputReksadana
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&reksadana).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputReksadana{
		Name:   reksadana.Name,
		Amount: reksadana.Amount,
		Return: reksadana.Reksadana_Return,
	}
	return output, nil
}

func DeleteReksadana(id int) (models.OutputReksadana, error) {
	var reksadana models.Reksadana
	var output models.OutputReksadana
	/*
		This is for finding the specific data that using filter by id, it is like query
		SELECT table_nameM table_name WHERE column_name = value;
	*/
	if err := config.DB.Find(&reksadana, "id=?", id).Error; err != nil {
		return output, err
	}
	/*
		This is for soft deleteing the specific data that using filter by id, it is like query
		UPDATE table_name SET deleted_at="...." WHERE id = ...;
	*/
	if err := config.DB.Delete(&reksadana, "id=?", id).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputReksadana{
		Name:   reksadana.Name,
		Amount: reksadana.Amount,
		Return: reksadana.Reksadana_Return,
	}
	return output, nil
}
