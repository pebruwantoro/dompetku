package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	mockDBUser = models.User{
		ID:        1,
		Username:  "user1",
		Email:     "user1@gmail.com",
		Password:  "1234",
		Token:     "",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBUser1 = models.User{
		ID:        1,
		Username:  "user1",
		Email:     "user1@gmail.com",
		Password:  "1234",
		Token:     "",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBUser2 = models.User{
		ID:        2,
		Username:  "user2",
		Email:     "user2@gmail.com",
		Password:  "5678",
		Token:     "",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestUserSignUp(t *testing.T) {
	config.InitDbTest()                              // connect to database
	config.DB.Migrator().DropTable(&models.User{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.User{}) // create table into database
	// inject User data from MockDBUser into User's table
	user, err := UserSignUp(mockDBUser)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBUser.Username, user.Username)
		assert.Equal(t, mockDBUser.Email, user.Email)
		assert.Equal(t, mockDBUser.Password, user.Password)
		assert.Equal(t, mockDBUser.Token, user.Token)
	}
	config.DB.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestUserSignIn(t *testing.T) {
	config.InitDbTest()                              // connect to database
	config.DB.Migrator().DropTable(&models.User{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.User{}) // create table into database
	// inject User data from MockDBUser into User's table
	user1, _ := UserSignUp(mockDBUser1)
	user, err := UserSignIn(user1.Email)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBUser1.Username, user.Username)
		assert.Equal(t, mockDBUser1.Email, user.Email)
		assert.Equal(t, mockDBUser.Password, user.Password)
	}
	config.DB.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}

func TestPasswordUSer(t *testing.T) {
	config.InitDbTest()                              // connect to database
	config.DB.Migrator().DropTable(&models.User{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.User{}) // create table into database
	// inject User data from MockDBUser into User's table
	UserSignUp(mockDBUser1)
	// check and test User data, if data injection exist in User's table database, test will be pass
	password, err := PasswordUser(mockDBUser1.Email)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBUser1.Password, password)
	}
}

func TestGetUserById(t *testing.T) {
	config.InitDbTest()                              // connect to database
	config.DB.Migrator().DropTable(&models.User{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.User{}) // create table into database
	// inject User data from MockDBUser into User's table
	UserSignUp(mockDBUser1)
	UserSignUp(mockDBUser2)
	// check and test User data, if data injection exist in User's table database, test will be pass
	user, err := GetUserById(1)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBUser1.Username, user.Username)
		assert.Equal(t, mockDBUser1.Email, user.Email)
		assert.Equal(t, mockDBUser1.Password, user.Password)
	}
}

func TestUpdateUser(t *testing.T) {
	config.InitDbTest()                              // connect to database
	config.DB.Migrator().DropTable(&models.User{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.User{}) // create table into database
	// inject User data from MockDBUser into User's table
	UserSignUp(mockDBUser1)
	UserSignUp(mockDBUser2)
	// check and test User data, if data injection exist in User's table database, test will be pass
	id := 1
	get_user, _ := GetUserById(id)
	get_user.Username = "user_new"
	get_user.Email = "new@gmail.com"
	get_user.Password = "0987654321"
	updated_user, err := UpdateUser(get_user)
	if assert.NoError(t, err) {
		assert.Equal(t, updated_user.Username, get_user.Username)
		assert.Equal(t, updated_user.Email, get_user.Email)
		assert.Equal(t, updated_user.Password, get_user.Password)
		assert.Equal(t, updated_user.Token, get_user.Token)
	}
	config.DB.Migrator().DropTable(&models.User{}) // Delete the table to make empty table
}
