package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"strconv"
)

func CreateProductiveInvoice(prod_invoice models.ProductiveInvoice) (models.OutputProductiveInvoice, error) {
	var output models.OutputProductiveInvoice
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&prod_invoice).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputProductiveInvoice{
		Name:   prod_invoice.Name,
		Amount: prod_invoice.Amount,
		Tenor:  prod_invoice.Tenor,
		Grade:  prod_invoice.Grade,
		Rate:   prod_invoice.Rate,
	}
	return output, nil
}

func GetAllProductiveInvoices() ([]models.OutputProductiveInvoice, error) {
	var prod_invoices []models.ProductiveInvoice
	var prod_invoices_output []models.OutputProductiveInvoice
	/*
		This is for finding and showing all of data from database, it is like query
		SELECT * FROM table_name;
	*/
	if err := config.DB.Find(&prod_invoices).Error; err != nil {
		return prod_invoices_output, err
	}
	//  create object for output
	for i := 0; i < len(prod_invoices); i++ {
		arr_prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoices[i].Name,
			Amount: prod_invoices[i].Amount,
			Tenor:  prod_invoices[i].Tenor,
			Grade:  prod_invoices[i].Grade,
			Rate:   prod_invoices[i].Rate,
		}
		prod_invoices_output = append(prod_invoices_output, arr_prod_invoices)
	}
	return prod_invoices_output, nil
}

func GetProductiveInvoiceByName(name string) ([]models.OutputProductiveInvoice, error) {
	var prod_invoice []models.ProductiveInvoice
	var prod_invoice_output []models.OutputProductiveInvoice
	/*
		This is for finding the specific data that using filter by name, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + name + "%"
	if err := config.DB.Find(&prod_invoice, "name LIKE ?", search).Error; err != nil {
		return prod_invoice_output, err
	}
	//  create object for output
	for i := 0; i < len(prod_invoice); i++ {
		prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoice[i].Name,
			Amount: prod_invoice[i].Amount,
			Tenor:  prod_invoice[i].Tenor,
			Grade:  prod_invoice[i].Grade,
			Rate:   prod_invoice[i].Rate,
		}
		prod_invoice_output = append(prod_invoice_output, prod_invoices)
	}
	return prod_invoice_output, nil
}

func GetProductiveInvoiceByTenor(tenor int) ([]models.OutputProductiveInvoice, error) {
	var prod_invoice []models.ProductiveInvoice
	var prod_invoice_output []models.OutputProductiveInvoice
	tenor_str := strconv.Itoa(tenor)
	/*
		This is for finding the specific data that using filter by tenor, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + tenor_str + "%"
	if err := config.DB.Find(&prod_invoice, "tenor LIKE ?", search).Error; err != nil {
		return prod_invoice_output, err
	}
	for i := 0; i < len(prod_invoice); i++ {
		prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoice[i].Name,
			Amount: prod_invoice[i].Amount,
			Tenor:  prod_invoice[i].Tenor,
			Grade:  prod_invoice[i].Grade,
			Rate:   prod_invoice[i].Rate,
		}
		prod_invoice_output = append(prod_invoice_output, prod_invoices)
	}
	return prod_invoice_output, nil
}

func GetProductiveInvoiceByGrade(grade string) ([]models.OutputProductiveInvoice, error) {
	var prod_invoice []models.ProductiveInvoice
	var prod_invoice_output []models.OutputProductiveInvoice
	/*
		This is for finding the specific data that using filter by grade, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + grade + "%"
	if err := config.DB.Find(&prod_invoice, "grade LIKE ?", search).Error; err != nil {
		return prod_invoice_output, err
	}
	//  create object for output
	for i := 0; i < len(prod_invoice); i++ {
		prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoice[i].Name,
			Amount: prod_invoice[i].Amount,
			Tenor:  prod_invoice[i].Tenor,
			Grade:  prod_invoice[i].Grade,
			Rate:   prod_invoice[i].Rate,
		}
		prod_invoice_output = append(prod_invoice_output, prod_invoices)
	}
	return prod_invoice_output, nil
}

func GetProductiveInvoiceByRate(rate int) ([]models.OutputProductiveInvoice, error) {
	var prod_invoice []models.ProductiveInvoice
	var prod_invoice_output []models.OutputProductiveInvoice
	rate_str := strconv.Itoa(rate)
	/*
		This is for finding the specific data that using filter by rate, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + rate_str + "%"
	if err := config.DB.Find(&prod_invoice, "rate LIKE ?", search).Error; err != nil {
		return prod_invoice_output, err
	}
	//  create object for output
	for i := 0; i < len(prod_invoice); i++ {
		prod_invoices := models.OutputProductiveInvoice{
			Name:   prod_invoice[i].Name,
			Amount: prod_invoice[i].Amount,
			Tenor:  prod_invoice[i].Tenor,
			Grade:  prod_invoice[i].Grade,
			Rate:   prod_invoice[i].Rate,
		}
		prod_invoice_output = append(prod_invoice_output, prod_invoices)
	}
	return prod_invoice_output, nil
}
func GetProductiveInvoiceById(id int) (models.ProductiveInvoice, error) {
	var prod_invoice models.ProductiveInvoice
	/*
		This if for finding the specific data that using filter by id, it is like query
		SELECT * FROM table_name WHERE column_name = value ORDER BY id LIMIT 1;
	*/
	if err := config.DB.Where("id=?", id).First(&prod_invoice).Error; err != nil {
		return prod_invoice, err
	}
	return prod_invoice, nil
}
func UpdateProductiveInvoice(prod_invoice models.ProductiveInvoice) (models.OutputProductiveInvoice, error) {
	var output models.OutputProductiveInvoice
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&prod_invoice).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputProductiveInvoice{
		Name:   prod_invoice.Name,
		Amount: prod_invoice.Amount,
		Tenor:  prod_invoice.Tenor,
		Grade:  prod_invoice.Grade,
		Rate:   prod_invoice.Rate,
	}
	return output, nil
}

func DeleteProductiveInvoice(id int) (models.OutputProductiveInvoice, error) {
	var prod_invoice models.ProductiveInvoice
	var output models.OutputProductiveInvoice
	/*
		This is for finding the specific data that using filter by id, it is like query
		SELECT table_nameM table_name WHERE column_name = value;
	*/
	if err := config.DB.Find(&prod_invoice, "id=?", id).Error; err != nil {
		return output, err
	}
	/*
		This is for soft deleteing the specific data that using filter by id, it is like query
		UPDATE table_name SET deleted_at="...." WHERE id = ...;
	*/
	if err := config.DB.Delete(&prod_invoice, "id=?", id).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputProductiveInvoice{
		Name:   prod_invoice.Name,
		Amount: prod_invoice.Amount,
		Tenor:  prod_invoice.Tenor,
		Grade:  prod_invoice.Grade,
		Rate:   prod_invoice.Rate,
	}
	return output, nil
}
