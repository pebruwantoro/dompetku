package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	mockDBReksadana = models.Reksadana{
		ID:               1,
		Name:             "Reksadana1",
		Amount:           100000,
		Reksadana_Return: 12,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
	mockDBReksadana1 = models.Reksadana{
		ID:               1,
		Name:             "Reksadana1",
		Amount:           100000,
		Reksadana_Return: 12,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
	mockDBReksadana2 = models.Reksadana{
		ID:               2,
		Name:             "Reksadana2",
		Amount:           100000,
		Reksadana_Return: 12,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
)

func TestCreateReksadana(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Reksadana{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Reksadana{}) // create table into database
	// inject Reksadana data from MockDBReksadana into Reksadana's table
	reksadana, err := CreateReksadana(mockDBReksadana)
	// check and test Reksadana data, if data injection exist in Reksadana's table database, test will be pass
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBReksadana.Name, reksadana.Name)
		assert.Equal(t, mockDBReksadana.Amount, reksadana.Amount)
		assert.Equal(t, mockDBReksadana.Reksadana_Return, reksadana.Return)
	}
	config.DB.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
}

func TestGetAllReksadanas(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Reksadana{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Reksadana{}) // create table into database
	// inject Reksadana data from MockDBReksadana into Reksadana's table
	CreateReksadana(mockDBReksadana1)
	CreateReksadana(mockDBReksadana2)
	// check and test Reksadana data, if data injection exist in Reksadana's table database, test will be pass
	reksadana, err := GetAllReksadana()
	var reksadana_output []models.OutputReksadana
	for i := 0; i < len(reksadana); i++ {
		arr_reksadana := models.OutputReksadana{
			Name:   reksadana[i].Name,
			Amount: reksadana[i].Amount,
			Return: reksadana[i].Return,
		}
		reksadana_output = append(reksadana_output, arr_reksadana)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBReksadana1.Name, reksadana_output[0].Name)
		assert.Equal(t, mockDBReksadana1.Amount, reksadana_output[0].Amount)
		assert.Equal(t, mockDBReksadana1.Reksadana_Return, reksadana_output[0].Return)
		assert.Equal(t, mockDBReksadana2.Name, reksadana_output[1].Name)
		assert.Equal(t, mockDBReksadana2.Amount, reksadana_output[1].Amount)
		assert.Equal(t, mockDBReksadana2.Reksadana_Return, reksadana_output[1].Return)
	}
	config.DB.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
}

func TestGetReksadanaByName(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Reksadana{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Reksadana{}) // create table into database
	// inject Reksadana data from MockDBReksadana into Reksadana's table
	CreateReksadana(mockDBReksadana1)
	CreateReksadana(mockDBReksadana2)
	// check and test Reksadana data, if data injection exist in Reksadana's table database, test will be pass
	name := "Reksadana"
	reksadana, err := GetReksadanaByName(name)
	var reksadana_output []models.OutputReksadana
	for i := 0; i < len(reksadana); i++ {
		arr_reksadana := models.OutputReksadana{
			Name:   reksadana[i].Name,
			Amount: reksadana[i].Amount,
			Return: reksadana[i].Return,
		}
		reksadana_output = append(reksadana_output, arr_reksadana)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBReksadana1.Name, reksadana_output[0].Name)
		assert.Equal(t, mockDBReksadana1.Amount, reksadana_output[0].Amount)
		assert.Equal(t, mockDBReksadana1.Reksadana_Return, reksadana_output[0].Return)
		assert.Equal(t, mockDBReksadana2.Name, reksadana_output[1].Name)
		assert.Equal(t, mockDBReksadana2.Amount, reksadana_output[1].Amount)
		assert.Equal(t, mockDBReksadana2.Reksadana_Return, reksadana_output[1].Return)
	}
	config.DB.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
}

func TestGetReksadanaByReturn(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Reksadana{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Reksadana{}) // create table into database
	// inject Reksadana data from MockDBReksadana into Reksadana's table
	CreateReksadana(mockDBReksadana1)
	CreateReksadana(mockDBReksadana2)
	// check and test Reksadana data, if data injection exist in Reksadana's table database, test will be pass
	input := 12
	reksadana, err := GetReksadanaByReturn(input)
	var reksadana_output []models.OutputReksadana
	for i := 0; i < len(reksadana); i++ {
		arr_reksadana := models.OutputReksadana{
			Name:   reksadana[i].Name,
			Amount: reksadana[i].Amount,
			Return: reksadana[i].Return,
		}
		reksadana_output = append(reksadana_output, arr_reksadana)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBReksadana1.Name, reksadana_output[0].Name)
		assert.Equal(t, mockDBReksadana1.Amount, reksadana_output[0].Amount)
		assert.Equal(t, mockDBReksadana1.Reksadana_Return, reksadana_output[0].Return)
		assert.Equal(t, mockDBReksadana2.Name, reksadana_output[1].Name)
		assert.Equal(t, mockDBReksadana2.Amount, reksadana_output[1].Amount)
		assert.Equal(t, mockDBReksadana2.Reksadana_Return, reksadana_output[1].Return)
	}
	config.DB.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
}

func TestGetReksadanaById(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Reksadana{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Reksadana{}) // create table into database
	// inject Reksadana data from MockDBReksadana into Reksadana's table
	CreateReksadana(mockDBReksadana1)
	CreateReksadana(mockDBReksadana2)
	// check and test Reksadana data, if data injection exist in Reksadana's table database, test will be pass
	id := 1
	reksadana, err := GetReksadanaById(id)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBReksadana1.Name, reksadana.Name)
		assert.Equal(t, mockDBReksadana1.Amount, reksadana.Amount)
		assert.Equal(t, mockDBReksadana1.Reksadana_Return, reksadana.Reksadana_Return)
	}
}

func TestUpdateReksadana(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Reksadana{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Reksadana{}) // create table into database
	// inject Reksadana data from MockDBReksadana into Reksadana's table
	CreateReksadana(mockDBReksadana1)
	CreateReksadana(mockDBReksadana2)
	// check and test Reksadana data, if data injection exist in Reksadana's table database, test will be pass
	id := 1
	get_Reksadana, _ := GetReksadanaById(id)
	get_Reksadana.Name = "Financing Baru"
	get_Reksadana.Amount = 100000000
	get_Reksadana.Reksadana_Return = 9
	updated_Reksadana, err := UpdateReksadana(get_Reksadana)
	if assert.NoError(t, err) {
		assert.Equal(t, updated_Reksadana.Name, get_Reksadana.Name)
		assert.Equal(t, updated_Reksadana.Amount, get_Reksadana.Amount)
		assert.Equal(t, updated_Reksadana.Return, get_Reksadana.Reksadana_Return)
	}
	config.DB.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
}

func TestDeleteReksadana(t *testing.T) {
	config.InitDbTest()                                   // connect to database
	config.DB.Migrator().DropTable(&models.Reksadana{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.Reksadana{}) // create table into database
	// inject Reksadana data from MockDBReksadana into Reksadana's table
	CreateReksadana(mockDBReksadana1)
	CreateReksadana(mockDBReksadana2)
	// check and test Reksadana data, if data injection exist in Reksadana's table database, test will be pass
	id := 2
	deleted_reksadana, err := DeleteReksadana(id)
	if assert.NoError(t, err) {
		assert.NotEqual(t, mockDBReksadana1, deleted_reksadana)
		assert.NotEqual(t, mockDBReksadana2, deleted_reksadana)
	}
	config.DB.Migrator().DropTable(&models.Reksadana{}) // Delete the table to make empty table
}
