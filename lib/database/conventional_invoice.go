package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"strconv"
)

func CreateConventionalInvoice(invoice models.ConventionalInvoice) (models.OutputConventionalInvoice, error) {
	var output models.OutputConventionalInvoice
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&invoice).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputConventionalInvoice{
		Name:   invoice.Name,
		Amount: invoice.Amount,
		Tenor:  invoice.Tenor,
		Grade:  invoice.Grade,
		Rate:   invoice.Rate,
	}
	return output, nil
}

func GetAllConventionalInvoices() ([]models.OutputConventionalInvoice, error) {
	var invoices []models.ConventionalInvoice
	var invoices_output []models.OutputConventionalInvoice
	/*
		This is for finding and showing all of data from database, it is like query
		SELECT * FROM table_name;
	*/
	if err := config.DB.Find(&invoices).Error; err != nil {
		return invoices_output, err
	}
	//  create object for output
	for i := 0; i < len(invoices); i++ {
		arr_invoices := models.OutputConventionalInvoice{
			Name:   invoices[i].Name,
			Amount: invoices[i].Amount,
			Tenor:  invoices[i].Tenor,
			Grade:  invoices[i].Grade,
			Rate:   invoices[i].Rate,
		}
		invoices_output = append(invoices_output, arr_invoices)
	}
	return invoices_output, nil
}

func GetConventionalInvoiceByName(name string) ([]models.OutputConventionalInvoice, error) {
	var invoice []models.ConventionalInvoice
	var invoice_output []models.OutputConventionalInvoice
	/*
		This is for finding the specific data that using filter by name, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + name + "%"
	if err := config.DB.Find(&invoice, "name LIKE ?", search).Error; err != nil {
		return invoice_output, err
	}
	//  create object for output
	for i := 0; i < len(invoice); i++ {
		invoices := models.OutputConventionalInvoice{
			Name:   invoice[i].Name,
			Amount: invoice[i].Amount,
			Tenor:  invoice[i].Tenor,
			Grade:  invoice[i].Grade,
			Rate:   invoice[i].Rate,
		}
		invoice_output = append(invoice_output, invoices)
	}
	return invoice_output, nil
}

func GetConventionalInvoiceByTenor(tenor int) ([]models.OutputConventionalInvoice, error) {
	var invoice []models.ConventionalInvoice
	var invoice_output []models.OutputConventionalInvoice
	tenor_str := strconv.Itoa(tenor)
	/*
		This is for finding the specific data that using filter by tenor, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + tenor_str + "%"
	if err := config.DB.Find(&invoice, "tenor LIKE ?", search).Error; err != nil {
		return invoice_output, err
	}
	//  create object for output
	for i := 0; i < len(invoice); i++ {
		invoices := models.OutputConventionalInvoice{
			Name:   invoice[i].Name,
			Amount: invoice[i].Amount,
			Tenor:  invoice[i].Tenor,
			Grade:  invoice[i].Grade,
			Rate:   invoice[i].Rate,
		}
		invoice_output = append(invoice_output, invoices)
	}
	return invoice_output, nil
}

func GetConventionalInvoiceByGrade(grade string) ([]models.OutputConventionalInvoice, error) {
	var invoice []models.ConventionalInvoice
	var invoice_output []models.OutputConventionalInvoice
	/*
		This is for finding the specific data that using filter by grade, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + grade + "%"
	if err := config.DB.Find(&invoice, "grade LIKE ?", search).Error; err != nil {
		return invoice_output, err
	}
	//  create object for output
	for i := 0; i < len(invoice); i++ {
		invoices := models.OutputConventionalInvoice{
			Name:   invoice[i].Name,
			Amount: invoice[i].Amount,
			Tenor:  invoice[i].Tenor,
			Grade:  invoice[i].Grade,
			Rate:   invoice[i].Rate,
		}
		invoice_output = append(invoice_output, invoices)
	}
	return invoice_output, nil
}

func GetConventionalInvoiceByRate(rate int) ([]models.OutputConventionalInvoice, error) {
	var invoice []models.ConventionalInvoice
	var invoice_output []models.OutputConventionalInvoice
	rate_str := strconv.Itoa(rate)
	/*
		This is for finding the specific data that using filter by rate, it is like query
		SELECT * FROM table_name WHERE column_name LIKE '%.....%';
	*/
	search := "%" + rate_str + "%"
	if err := config.DB.Find(&invoice, "rate LIKE ?", search).Error; err != nil {
		return invoice_output, err
	}
	//  create object for output
	for i := 0; i < len(invoice); i++ {
		invoices := models.OutputConventionalInvoice{
			Name:   invoice[i].Name,
			Amount: invoice[i].Amount,
			Tenor:  invoice[i].Tenor,
			Grade:  invoice[i].Grade,
			Rate:   invoice[i].Rate,
		}
		invoice_output = append(invoice_output, invoices)
	}
	return invoice_output, nil
}
func GetConventionalInvoiceById(id int) (models.ConventionalInvoice, error) {
	var invoice models.ConventionalInvoice
	/*
		This if for finding the specific data that using filter by id, it is like query
		SELECT * FROM table_name WHERE column_name = value ORDER BY id LIMIT 1;
	*/
	if err := config.DB.Where("id=?", id).First(&invoice).Error; err != nil {
		return invoice, err
	}
	return invoice, nil
}
func UpdateConventionalInvoice(invoice models.ConventionalInvoice) (models.OutputConventionalInvoice, error) {
	var output models.OutputConventionalInvoice
	/*
		This is for saving the data into database, it is like query
		INSERT INTO table_name(column1, ...) VALUE(value1,....);
	*/
	if err := config.DB.Save(&invoice).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputConventionalInvoice{
		Name:   invoice.Name,
		Amount: invoice.Amount,
		Tenor:  invoice.Tenor,
		Grade:  invoice.Grade,
		Rate:   invoice.Rate,
	}
	return output, nil
}

func DeleteConventionalInvoice(id int) (models.OutputConventionalInvoice, error) {
	var invoice models.ConventionalInvoice
	var output models.OutputConventionalInvoice
	/*
		This is for finding the specific data that using filter by id, it is like query
		SELECT table_nameM table_name WHERE column_name = value;
	*/
	if err := config.DB.Find(&invoice, "id=?", id).Error; err != nil {
		return output, err
	}
	/*
		This is for soft deleteing the specific data that using filter by id, it is like query
		UPDATE table_name SET deleted_at="...." WHERE id = ...;
	*/
	if err := config.DB.Delete(&invoice, "id=?", id).Error; err != nil {
		return output, err
	}
	//  create object for output
	output = models.OutputConventionalInvoice{
		Name:   invoice.Name,
		Amount: invoice.Amount,
		Tenor:  invoice.Tenor,
		Grade:  invoice.Grade,
		Rate:   invoice.Rate,
	}
	return output, nil
}
