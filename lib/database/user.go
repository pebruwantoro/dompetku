package database

import (
	"app/dompetku/config"
	"app/dompetku/middlewares"
	"app/dompetku/models"
)

// Function for SignUp new user account
func UserSignUp(user models.User) (models.User, error) {
	if err := config.DB.Save(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}

// Function for SignIn user
func UserSignIn(email string) (models.User, error) {
	var user models.User
	var err error
	if err = config.DB.Where("email = ?", email).First(&user).Error; err != nil {
		return user, err
	}
	// Create JWT Token for Authentication User
	user.Token, err = middlewares.CreateToken(int(user.ID))
	if err != nil {
		return user, err
	}
	if err := config.DB.Save(user).Error; err != nil {
		return user, err
	}

	return user, nil
}

// Function for searching user password
func PasswordUser(email string) (string, error) {
	var user models.User
	if err := config.DB.Where("email = ?", email).First(&user).Error; err != nil {
		return user.Password, err
	}
	return user.Password, nil
}

// Function for getting user data by id from database
func GetUserById(id int) (models.User, error) {
	var user models.User
	if err := config.DB.Where("id=?", id).First(&user).Error; err != nil {
		return user, err
	}

	return user, nil
}

// Function for updating user data
func UpdateUser(user models.User) (models.User, error) {
	if err := config.DB.Save(&user).Error; err != nil {
		return user, err
	}
	return user, nil
}
