package database

import (
	"app/dompetku/config"
	"app/dompetku/models"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var (
	mockDBConventionalInvoice = models.ConventionalInvoice{
		ID:        1,
		Name:      "Invoice1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBConventionalInvoice1 = models.ConventionalInvoice{
		ID:        1,
		Name:      "Invoice1",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	mockDBConventionalInvoice2 = models.ConventionalInvoice{
		ID:        2,
		Name:      "Invoice2",
		Amount:    100000,
		Tenor:     12,
		Grade:     "A",
		Rate:      3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
)

func TestCreateConventionalInvoice(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	invoice, err := CreateConventionalInvoice(mockDBConventionalInvoice)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalInvoice.Name, invoice.Name)
		assert.Equal(t, mockDBConventionalInvoice.Amount, invoice.Amount)
		assert.Equal(t, mockDBConventionalInvoice.Tenor, invoice.Tenor)
		assert.Equal(t, mockDBConventionalInvoice.Grade, invoice.Grade)
		assert.Equal(t, mockDBConventionalInvoice.Rate, invoice.Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
}

func TestGetAllConventionalInvoices(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	CreateConventionalInvoice(mockDBConventionalInvoice1)
	CreateConventionalInvoice(mockDBConventionalInvoice2)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	invoices, err := GetAllConventionalInvoices()
	var invoices_output []models.OutputConventionalInvoice
	for i := 0; i < len(invoices); i++ {
		arr_invoices := models.OutputConventionalInvoice{
			Name:   invoices[i].Name,
			Amount: invoices[i].Amount,
			Tenor:  invoices[i].Tenor,
			Grade:  invoices[i].Grade,
			Rate:   invoices[i].Rate,
		}
		invoices_output = append(invoices_output, arr_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalInvoice1.Name, invoices_output[0].Name)
		assert.Equal(t, mockDBConventionalInvoice1.Amount, invoices_output[0].Amount)
		assert.Equal(t, mockDBConventionalInvoice1.Tenor, invoices_output[0].Tenor)
		assert.Equal(t, mockDBConventionalInvoice1.Grade, invoices_output[0].Grade)
		assert.Equal(t, mockDBConventionalInvoice1.Rate, invoices_output[0].Rate)
		assert.Equal(t, mockDBConventionalInvoice2.Name, invoices_output[1].Name)
		assert.Equal(t, mockDBConventionalInvoice2.Amount, invoices_output[1].Amount)
		assert.Equal(t, mockDBConventionalInvoice2.Tenor, invoices_output[1].Tenor)
		assert.Equal(t, mockDBConventionalInvoice2.Grade, invoices_output[1].Grade)
		assert.Equal(t, mockDBConventionalInvoice2.Rate, invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
}

func TestGetConventionalInvoiceByName(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	CreateConventionalInvoice(mockDBConventionalInvoice1)
	CreateConventionalInvoice(mockDBConventionalInvoice2)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	name := "invoice"
	invoices, err := GetConventionalInvoiceByName(name)
	var invoices_output []models.OutputConventionalInvoice
	for i := 0; i < len(invoices); i++ {
		arr_invoices := models.OutputConventionalInvoice{
			Name:   invoices[i].Name,
			Amount: invoices[i].Amount,
			Tenor:  invoices[i].Tenor,
			Grade:  invoices[i].Grade,
			Rate:   invoices[i].Rate,
		}
		invoices_output = append(invoices_output, arr_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalInvoice1.Name, invoices_output[0].Name)
		assert.Equal(t, mockDBConventionalInvoice1.Amount, invoices_output[0].Amount)
		assert.Equal(t, mockDBConventionalInvoice1.Tenor, invoices_output[0].Tenor)
		assert.Equal(t, mockDBConventionalInvoice1.Grade, invoices_output[0].Grade)
		assert.Equal(t, mockDBConventionalInvoice1.Rate, invoices_output[0].Rate)
		assert.Equal(t, mockDBConventionalInvoice2.Name, invoices_output[1].Name)
		assert.Equal(t, mockDBConventionalInvoice2.Amount, invoices_output[1].Amount)
		assert.Equal(t, mockDBConventionalInvoice2.Tenor, invoices_output[1].Tenor)
		assert.Equal(t, mockDBConventionalInvoice2.Grade, invoices_output[1].Grade)
		assert.Equal(t, mockDBConventionalInvoice2.Rate, invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
}

func TestGetConventionalInvoiceByTenor(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	CreateConventionalInvoice(mockDBConventionalInvoice1)
	CreateConventionalInvoice(mockDBConventionalInvoice2)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	tenor := 12
	invoices, err := GetConventionalInvoiceByTenor(tenor)
	var invoices_output []models.OutputConventionalInvoice
	for i := 0; i < len(invoices); i++ {
		arr_invoices := models.OutputConventionalInvoice{
			Name:   invoices[i].Name,
			Amount: invoices[i].Amount,
			Tenor:  invoices[i].Tenor,
			Grade:  invoices[i].Grade,
			Rate:   invoices[i].Rate,
		}
		invoices_output = append(invoices_output, arr_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalInvoice1.Name, invoices_output[0].Name)
		assert.Equal(t, mockDBConventionalInvoice1.Amount, invoices_output[0].Amount)
		assert.Equal(t, mockDBConventionalInvoice1.Tenor, invoices_output[0].Tenor)
		assert.Equal(t, mockDBConventionalInvoice1.Grade, invoices_output[0].Grade)
		assert.Equal(t, mockDBConventionalInvoice1.Rate, invoices_output[0].Rate)
		assert.Equal(t, mockDBConventionalInvoice2.Name, invoices_output[1].Name)
		assert.Equal(t, mockDBConventionalInvoice2.Amount, invoices_output[1].Amount)
		assert.Equal(t, mockDBConventionalInvoice2.Tenor, invoices_output[1].Tenor)
		assert.Equal(t, mockDBConventionalInvoice2.Grade, invoices_output[1].Grade)
		assert.Equal(t, mockDBConventionalInvoice2.Rate, invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
}

func TestGetConventionalInvoiceByGrade(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	CreateConventionalInvoice(mockDBConventionalInvoice1)
	CreateConventionalInvoice(mockDBConventionalInvoice2)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	grade := "A"
	invoices, err := GetConventionalInvoiceByGrade(grade)
	var invoices_output []models.OutputConventionalInvoice
	for i := 0; i < len(invoices); i++ {
		arr_invoices := models.OutputConventionalInvoice{
			Name:   invoices[i].Name,
			Amount: invoices[i].Amount,
			Tenor:  invoices[i].Tenor,
			Grade:  invoices[i].Grade,
			Rate:   invoices[i].Rate,
		}
		invoices_output = append(invoices_output, arr_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalInvoice1.Name, invoices_output[0].Name)
		assert.Equal(t, mockDBConventionalInvoice1.Amount, invoices_output[0].Amount)
		assert.Equal(t, mockDBConventionalInvoice1.Tenor, invoices_output[0].Tenor)
		assert.Equal(t, mockDBConventionalInvoice1.Grade, invoices_output[0].Grade)
		assert.Equal(t, mockDBConventionalInvoice1.Rate, invoices_output[0].Rate)
		assert.Equal(t, mockDBConventionalInvoice2.Name, invoices_output[1].Name)
		assert.Equal(t, mockDBConventionalInvoice2.Amount, invoices_output[1].Amount)
		assert.Equal(t, mockDBConventionalInvoice2.Tenor, invoices_output[1].Tenor)
		assert.Equal(t, mockDBConventionalInvoice2.Grade, invoices_output[1].Grade)
		assert.Equal(t, mockDBConventionalInvoice2.Rate, invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
}

func TestGetConventionalInvoiceByRate(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	CreateConventionalInvoice(mockDBConventionalInvoice1)
	CreateConventionalInvoice(mockDBConventionalInvoice2)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	rate := 3
	invoices, err := GetConventionalInvoiceByRate(rate)
	var invoices_output []models.OutputConventionalInvoice
	for i := 0; i < len(invoices); i++ {
		arr_invoices := models.OutputConventionalInvoice{
			Name:   invoices[i].Name,
			Amount: invoices[i].Amount,
			Tenor:  invoices[i].Tenor,
			Grade:  invoices[i].Grade,
			Rate:   invoices[i].Rate,
		}
		invoices_output = append(invoices_output, arr_invoices)
	}
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalInvoice1.Name, invoices_output[0].Name)
		assert.Equal(t, mockDBConventionalInvoice1.Amount, invoices_output[0].Amount)
		assert.Equal(t, mockDBConventionalInvoice1.Tenor, invoices_output[0].Tenor)
		assert.Equal(t, mockDBConventionalInvoice1.Grade, invoices_output[0].Grade)
		assert.Equal(t, mockDBConventionalInvoice1.Rate, invoices_output[0].Rate)
		assert.Equal(t, mockDBConventionalInvoice2.Name, invoices_output[1].Name)
		assert.Equal(t, mockDBConventionalInvoice2.Amount, invoices_output[1].Amount)
		assert.Equal(t, mockDBConventionalInvoice2.Tenor, invoices_output[1].Tenor)
		assert.Equal(t, mockDBConventionalInvoice2.Grade, invoices_output[1].Grade)
		assert.Equal(t, mockDBConventionalInvoice2.Rate, invoices_output[1].Rate)
	}
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
}

func TestGetConventionalInvoiceById(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	CreateConventionalInvoice(mockDBConventionalInvoice1)
	CreateConventionalInvoice(mockDBConventionalInvoice2)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	id := 1
	invoice, err := GetConventionalInvoiceById(id)
	if assert.NoError(t, err) {
		assert.Equal(t, mockDBConventionalInvoice1.Name, invoice.Name)
		assert.Equal(t, mockDBConventionalInvoice1.Amount, invoice.Amount)
		assert.Equal(t, mockDBConventionalInvoice1.Tenor, invoice.Tenor)
		assert.Equal(t, mockDBConventionalInvoice1.Grade, invoice.Grade)
	}
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
}

func TestUpdateConventionalInvoice(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	CreateConventionalInvoice(mockDBConventionalInvoice1)
	CreateConventionalInvoice(mockDBConventionalInvoice2)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	id := 1
	get_invoice, _ := GetConventionalInvoiceById(id)
	get_invoice.Name = "Invoice Baru"
	get_invoice.Amount = 100000000
	get_invoice.Tenor = 12
	get_invoice.Grade = "E"
	updated_invoice, err := UpdateConventionalInvoice(get_invoice)
	if assert.NoError(t, err) {
		assert.Equal(t, updated_invoice.Name, get_invoice.Name)
		assert.Equal(t, updated_invoice.Amount, get_invoice.Amount)
		assert.Equal(t, updated_invoice.Tenor, get_invoice.Tenor)
		assert.Equal(t, updated_invoice.Grade, get_invoice.Grade)
	}
}

func TestDeleteConventionalInvoice(t *testing.T) {
	config.InitDbTest()                                             // connect to database
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{})   // Delete the table to make empty table
	config.DB.Migrator().AutoMigrate(&models.ConventionalInvoice{}) // create table into database
	// inject ConventionalInvoice data from MockDBConventionalInvoice into ConventionalInvoice's table
	CreateConventionalInvoice(mockDBConventionalInvoice1)
	CreateConventionalInvoice(mockDBConventionalInvoice2)
	// check and test ConventionalInvoice data, if data injection exist in ConventionalInvoice's table database, test will be pass
	id := 2
	deleted_invoice, err := DeleteConventionalInvoice(id)
	if assert.NoError(t, err) {
		assert.NotEqual(t, mockDBConventionalInvoice1, deleted_invoice)
		assert.NotEqual(t, mockDBConventionalInvoice2, deleted_invoice)
	}
	config.DB.Migrator().DropTable(&models.ConventionalInvoice{}) // Delete the table to make empty table
}
