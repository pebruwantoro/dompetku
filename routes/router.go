package routes

import (
	"app/dompetku/constant"
	"app/dompetku/controllers"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func New(e *echo.Echo) {
	//--------------------User Controllers--------------------//
	e.POST("/signup", controllers.UserSignUpController) // user SignUp
	e.POST("/signin", controllers.UserSignInController) // user SignIn

	//--------------------Authorization--------------------//
	eJwt := e.Group("")
	eJwt.Use(middleware.JWT([]byte(constant.SECRET_JWT)))
	//--------------------User SingOut--------------------//
	eJwt.PUT("/signout/:user_id", controllers.UserSignOutController) // User SignOut
	//--------------------Update User Data--------------------//
	eJwt.PUT("/users/:user_id", controllers.UpdateUserController) // update user data

	//--------------------CRUD Conventional Invoice Controller--------------------//
	eJwt.POST("/conventional_invoice", controllers.CreateConventionalInvoiceController)                         // create conventional invoice
	eJwt.GET("/conventional_invoice", controllers.GetAllConventionalInvoiceController)                          // get all conventional invoice
	eJwt.GET("/conventional_invoice/name/:invoice_name", controllers.GetConventionalInvoiceByNameController)    // find conventional invoice filter by name
	eJwt.GET("/conventional_invoice/tenor/:invoice_tenor", controllers.GetConventionalInvoiceByTenorController) // find conventional invoice filter by tenor
	eJwt.GET("/conventional_invoice/grade/:invoice_grade", controllers.GetConventionalInvoiceByGradeController) // find conventional invoice filter by grade
	eJwt.GET("/conventional_invoice/rate/:invoice_rate", controllers.GetConventionalInvoiceByRateController)    // find conventional invoice filter by rate
	eJwt.PUT("/conventional_invoice/:invoice_id", controllers.UpdateConventionalInvoiceController)              // update conventional invoice data
	eJwt.DELETE("/conventional_invoice/:invoice_id", controllers.DeleteConventionalInvoiceController)           // delete conventional invoice data

	//--------------------CRUD Conventional OSF Controller--------------------//
	eJwt.POST("/conventional_osf", controllers.CreateConventionalOSFController)                     // create conventional osf
	eJwt.GET("/conventional_osf", controllers.GetAllConventionalOSFController)                      // get all conventional osf
	eJwt.GET("/conventional_osf/name/:osf_name", controllers.GetConventionalOSFByNameController)    // find conventional osf filter by name
	eJwt.GET("/conventional_osf/tenor/:osf_tenor", controllers.GetConventionalOSFByTenorController) // find conventional osf filter by tenor
	eJwt.GET("/conventional_osf/grade/:osf_grade", controllers.GetConventionalOSFByGradeController) // find conventional osf filter by grade
	eJwt.GET("/conventional_osf/rate/:osf_rate", controllers.GetConventionalOSFByRateController)    // find conventional osf filter by rate
	eJwt.PUT("/conventional_osf/:osf_id", controllers.UpdateConventionalOSFController)              // update conventional osf data
	eJwt.DELETE("/conventional_osf/:osf_id", controllers.DeleteConventionalOSFController)           // delete conventional osf data

	//------------------CRUD Productive Invoice Controller--------------------//
	eJwt.POST("/productive_invoice", controllers.CreateProductiveInvoiceController)                              // create productive invoice
	eJwt.GET("/productive_invoice", controllers.GetAllProductiveInvoiceController)                               // get all productive invoice
	eJwt.GET("/productive_invoice/name/:prod_invoice_name", controllers.GetProductiveInvoiceByNameController)    // find productive invoice filter by name
	eJwt.GET("/productive_invoice/tenor/:prod_invoice_tenor", controllers.GetProductiveInvoiceByTenorController) // find productive invoice filter by tenor
	eJwt.GET("/productive_invoice/grade/:prod_invoice_grade", controllers.GetProductiveInvoiceByGradeController) // find productive invoice filter by grade
	eJwt.GET("/productive_invoice/rate/:prod_invoice_rate", controllers.GetProductiveInvoiceByRateController)    // find productive invoice filter by rate
	eJwt.PUT("/productive_invoice/:prod_invoice_id", controllers.UpdateProductiveInvoiceController)              // update productive invoice data
	eJwt.DELETE("/productive_invoice/:prod_invoice_id", controllers.DeleteProductiveInvoiceController)           // delete productive invoice data

	//------------------CRUD SBN Controller--------------------//
	eJwt.POST("/sbn", controllers.CreateSBNController)                     // create SBN
	eJwt.GET("/sbn", controllers.GetAllSBNController)                      // get all SBN
	eJwt.GET("/sbn/name/:sbn_name", controllers.GetSBNByNameController)    // find SBN filter by name
	eJwt.GET("/sbn/tenor/:sbn_tenor", controllers.GetSBNByTenorController) // find SBN filter by tenor
	eJwt.GET("/sbn/rate/:sbn_rate", controllers.GetSBNByRateController)    // find SBN filter by rate
	eJwt.GET("/sbn/type/:sbn_type", controllers.GetSBNByTypeController)    // find SBN filter by type
	eJwt.PUT("/sbn/:sbn_id", controllers.UpdateSBNController)              // update SBN data
	eJwt.DELETE("/sbn/:sbn_id", controllers.DeleteSBNController)           // delete SBN data

	//------------------CRUD Reksadana Controller--------------------//
	eJwt.POST("/reksadana", controllers.CreateReksadanaController)                              // create reksadana
	eJwt.GET("/reksadana", controllers.GetAllReksadanaController)                               // get all reksadana
	eJwt.GET("/reksadana/name/:reksadana_name", controllers.GetReksadanaByNameController)       // find reksadana filter by name
	eJwt.GET("/reksadana/return/:reksadana_return", controllers.GetReksadanaByReturnController) // find reksadana filter by return
	eJwt.PUT("/reksadana/:reksadana_id", controllers.UpdateReksadanaController)                 // update reksadana data
	eJwt.DELETE("/reksadana/:reksadana_id", controllers.DeleteReksadanaController)              // delete reksadana data

	//------------------CRUD financing Controller--------------------//
	eJwt.POST("/financing", controllers.CreateFinancingController)                        // create financing
	eJwt.GET("/financing", controllers.GetAllFinancingController)                         // get all financing
	eJwt.GET("/financing/name/:financing_name", controllers.GetFinancingByNameController) // find financing filter by name
	eJwt.GET("/financing/sub/:financing_sub", controllers.GetFinancingBySubController)    // find financing filter by sub
	eJwt.PUT("/financing/:financing_id", controllers.UpdateFinancingController)           // update financing data
	eJwt.DELETE("/financing/:financing_id", controllers.DeleteFinancingController)        // delete financing data

}
