#!bin/bash
go test -v -coverpkg=./... -coverprofile=profile.cov ./...
go tool cover -func profile.cov
go tool cover -html profile.cov