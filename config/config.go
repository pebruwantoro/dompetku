package config

import (
	"app/dompetku/models"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB
var HTTP_PORT int

func InitDb() {
	connectionString := "root:02021996Doni*@tcp(mysql:3306)/project?charset=utf8&parseTime=True&loc=Local" //
	var err error
	DB, err = gorm.Open(mysql.Open(connectionString), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	InitMigrate()
}

func InitDbTest() {
	connectionString := "root:02021996Doni*@tcp(localhost:3306)/testing?charset=utf8&parseTime=True&loc=Local" //
	var err error
	DB, err = gorm.Open(mysql.Open(connectionString), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	InitMigrate()
}

func InitPort() {
	HTTP_PORT = 8080
}

func InitMigrate() {
	DB.AutoMigrate(&models.ConventionalInvoice{})
	DB.AutoMigrate(&models.ConventionalOSF{})
	DB.AutoMigrate(&models.Financing{})
	DB.AutoMigrate(&models.ProductiveInvoice{})
	DB.AutoMigrate(&models.Reksadana{})
	DB.AutoMigrate(&models.SBN{})
	DB.AutoMigrate(&models.User{})
}

func ConfigTest() (*gorm.DB, error) {
	var err error
	connectionStringTest := "root:02021996Doni*@tcp(localhost:3306)/testing_controller?charset=utf8&parseTime=True&loc=Local"
	DB, err = gorm.Open(mysql.Open(connectionStringTest), &gorm.Config{})
	if err != nil {
		return DB, err
	}
	return DB, err
}
