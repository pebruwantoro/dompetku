package models

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID        uint   `gorm:"type:int; primaryKey"`
	Username  string `gorm:"type:varchar(16); unique; not null" json:"username" form:"username"`
	Email     string `gorm:"type:varchar(50); unique; not null" json:"email" form:"email"`
	Password  string `gorm:"not null" json:"password" form:"password"`
	Token     string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
