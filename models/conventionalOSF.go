package models

import (
	"time"

	"gorm.io/gorm"
)

type ConventionalOSF struct {
	ID        uint `gorm:"type:int; primaryKey; unique; not null" json:"id" form:"id"`
	Name      string
	Amount    int
	Tenor     int
	Grade     string
	Rate      int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type OutputConventionalOSF struct {
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Tenor  int    `json:"tenor"`
	Grade  string `json:"grade"`
	Rate   int    `json:"rate"`
}
