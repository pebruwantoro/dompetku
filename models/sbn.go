package models

import (
	"time"

	"gorm.io/gorm"
)

type SBN struct {
	ID        uint `gorm:"type: int; primaryKey;"`
	Name      string
	Amount    int
	Tenor     int
	Rate      int
	Type      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type OutputSBN struct {
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Tenor  int    `json:"tenor"`
	Rate   int    `json:"rate"`
	Type   string `json:"type"`
}
