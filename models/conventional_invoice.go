package models

import (
	"time"

	"gorm.io/gorm"
)

type ConventionalInvoice struct {
	ID        uint `gorm:"type:int; primaryKey"`
	Name      string
	Amount    int
	Tenor     int
	Grade     string
	Rate      int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type OutputConventionalInvoice struct {
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Tenor  int    `json:"tenor"`
	Grade  string `json:"grade"`
	Rate   int    `json:"rate"`
}
