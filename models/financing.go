package models

import (
	"time"

	"gorm.io/gorm"
)

type Financing struct {
	ID        uint `gorm:"type:int; primaryKey;"`
	Name      string
	Count     int
	Sub       string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

type OutputFinancing struct {
	Name  string `json:"name"`
	Count int    `json:"count"`
	Sub   string `json:"sub"`
}
