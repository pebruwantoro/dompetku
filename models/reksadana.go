package models

import (
	"time"

	"gorm.io/gorm"
)

type Reksadana struct {
	ID               uint `gorm:"type:int; primaryKey;"`
	Name             string
	Amount           int
	Reksadana_Return int
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        gorm.DeletedAt `gorm:"index"`
}

type OutputReksadana struct {
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Return int    `json:"return"`
}
